TEMPLATE = lib
TARGET = ukui-sidebar-items

QT += gui qml quick
CONFIG += plugin c++11

TARGET = $$qtLibraryTarget($$TARGET)

# 插件uri
PLUGIN_IMPORT_URI = org.ukui.sidebar.items
DEFINES += PLUGIN_IMPORT_URI='\\"$${PLUGIN_IMPORT_URI}\\"'

# 导入gsetting相关依赖
CONFIG += link_pkgconfig
PKGCONFIG += gsettings-qt xcb

# opencv
INCLUDEPATH += /usr/include/opencv4/
LIBS += -lopencv_core -lopencv_imgcodecs -lopencv_imgproc

# qml模块源码
SOURCES += \
        $$PWD/sidebar-public-plugin.cpp \
        $$PWD/theme-palette.cpp

HEADERS += \
        $$PWD/sidebar-public-plugin.h \
        $$PWD/theme-palette.h

include(items/items.pri)

# 模块描述文件
qmldir.files = $$PWD/qmldir
DISTFILES += $$PWD/qmldir

qmlFiles.files = $$files($$PWD/qml/*.qml)
DISTFILES += $$qmlFiles.files

# 安装模块文件
unix {
    installPath = $$[QT_INSTALL_QML]/$$replace(PLUGIN_IMPORT_URI, \., /)
    qmldir.path = $$installPath
    target.path = $$installPath
    qmlFiles.path = $$installPath/qml

    INSTALLS += target qmldir qmlFiles
}
