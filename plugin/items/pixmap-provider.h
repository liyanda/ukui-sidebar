/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: hxf <hewenfei@kylinos.cn>
 *
 */

//
// Created by hxf on 22-11-9.
//

#ifndef UKUI_SIDEBAR_PIXMAP_PROVIDER_H
#define UKUI_SIDEBAR_PIXMAP_PROVIDER_H

#include <QString>
#include <QPixmap>
#include <QGSettings>

#include "commons.h"

class PixmapProvider : public QObject
{
    Q_OBJECT
public:
    static PixmapProvider *instance();
    const QString &pictureOption();
    QPixmap &getPixmap(BackgroundType::Type type);
    void loadPixmap(BackgroundType::Type type);

private:
    explicit PixmapProvider(QObject *parent = nullptr);

    void loadDesktopBackground();
    void loadScreensaverPic();
    static void blurPixmap(const QString &imageFile, QPixmap &saveToPixmap);
    static void blurPixmap(const QString &imageFile, const QString &color, QPixmap &saveToPixmap);
    static void blurPixmap(QImage &image, QPixmap &saveToPixmap);

private Q_SLOTS:
    void desktopPicChangedSlot(const QString& key);
    void screensaverPicChangedSlot(const QString& key);

Q_SIGNALS:
    void pixmapChanged(BackgroundType::Type);

private:
    QString m_primaryColor;
    QString m_pictureOptions;
    QString m_desktopFileName;
    QString m_screensaverFileName;

    QGSettings *m_backgroundGSetting = nullptr;
    QGSettings *m_screensaverGSetting = nullptr;

    QPixmap m_desktopPixmap;
    QPixmap m_screensaverPixmap;
};

#endif //UKUI_SIDEBAR_PIXMAP_PROVIDER_H
