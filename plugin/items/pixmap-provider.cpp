/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: hxf <hewenfei@kylinos.cn>
 *
 */

//
// Created by hxf on 22-11-9.
//

#include "pixmap-provider.h"

#include <QImage>
#include <QFile>
#include <QVariant>
#include <QPainter>

//opencv
#include <opencv2/imgproc.hpp>
#include <opencv2/core/mat.hpp>

#define BACKGROUND_SETTING_SCHEMA  "org.mate.background"
#define BACKGROUND_FILENAME        "pictureFilename"
#define PRIMARY_COLOR              "primaryColor"
#define PICTURE_OPTIONS            "pictureOptions"
#define DEFAULT_SIZE               {1920, 1080}

#define SCREENSAVER_SETTING_SCHEMA "org.ukui.screensaver"
#define SCREENSAVER_FILENAME       "background"

static PixmapProvider *globalInstance = nullptr;

PixmapProvider *PixmapProvider::instance()
{
    if (!globalInstance) {
        globalInstance = new PixmapProvider(nullptr);
    }
    return globalInstance;
}

PixmapProvider::PixmapProvider(QObject *parent) : QObject(parent)
{

}

void PixmapProvider::loadDesktopBackground()
{
    if (m_backgroundGSetting) {
        return;
    }

    QByteArray schema = QByteArray(BACKGROUND_SETTING_SCHEMA);
    if (QGSettings::isSchemaInstalled(schema)) {
        m_backgroundGSetting = new QGSettings(schema, "", this);
        if (m_backgroundGSetting->keys().contains(BACKGROUND_FILENAME)) {
            m_desktopFileName = m_backgroundGSetting->get(BACKGROUND_FILENAME).toString();
        }

        if (m_backgroundGSetting->keys().contains(PRIMARY_COLOR)) {
            m_primaryColor = m_backgroundGSetting->get(PRIMARY_COLOR).toString();
        }

        if (m_backgroundGSetting->keys().contains(PICTURE_OPTIONS)) {
            m_pictureOptions = m_backgroundGSetting->get(PICTURE_OPTIONS).toString();
        }

        connect(m_backgroundGSetting, &QGSettings::changed,
                this, &PixmapProvider::desktopPicChangedSlot);
    }

    blurPixmap(m_desktopFileName, m_primaryColor, m_desktopPixmap);
}

void PixmapProvider::loadScreensaverPic()
{
    if (m_screensaverGSetting) {
        return;
    }

    QByteArray schema = QByteArray(SCREENSAVER_SETTING_SCHEMA);
    if (QGSettings::isSchemaInstalled(schema)) {
        m_screensaverGSetting = new QGSettings(schema, "", this);
        if (m_screensaverGSetting->keys().contains(SCREENSAVER_FILENAME)) {
            m_screensaverFileName = m_screensaverGSetting->get(SCREENSAVER_FILENAME).toString();
        }

        connect(m_screensaverGSetting, &QGSettings::changed,
                this, &PixmapProvider::screensaverPicChangedSlot);
    }

    blurPixmap(m_screensaverFileName, m_screensaverPixmap);
}

void PixmapProvider::desktopPicChangedSlot(const QString& key)
{
    if (key == BACKGROUND_FILENAME || key == PRIMARY_COLOR || key == PICTURE_OPTIONS) {
        if (key == BACKGROUND_FILENAME) {
            m_desktopFileName = m_backgroundGSetting->get(BACKGROUND_FILENAME).toString();
        } else if (key == PICTURE_OPTIONS) {
            m_pictureOptions = m_backgroundGSetting->get(PICTURE_OPTIONS).toString();
        } else {
            m_primaryColor = m_backgroundGSetting->get(PRIMARY_COLOR).toString();
        }

        blurPixmap(m_desktopFileName, m_primaryColor, m_desktopPixmap);
        Q_EMIT pixmapChanged(BackgroundType::DesktopPic);
    }
}

void PixmapProvider::screensaverPicChangedSlot(const QString& key)
{
    if (key == SCREENSAVER_FILENAME) {
        m_screensaverFileName = m_screensaverGSetting->get(SCREENSAVER_FILENAME).toString();
        blurPixmap(m_screensaverFileName, m_screensaverPixmap);
        Q_EMIT pixmapChanged(BackgroundType::ScreensaverPic);
    }
}

void PixmapProvider::blurPixmap(const QString& imageFile, QPixmap &saveToPixmap)
{
    QImage image;
    if (imageFile.isEmpty() || !QFile::exists(imageFile)) {
        image = QImage(DEFAULT_SIZE, QImage::Format_ARGB32_Premultiplied);
        image.fill(DEFAULT_COLOR);
    } else {
        image = QImage(imageFile);
    }

    blurPixmap(image, saveToPixmap);
}

void PixmapProvider::blurPixmap(const QString &imageFile, const QString &color, QPixmap &saveToPixmap)
{
    QImage image;
    if (imageFile.isEmpty() || !QFile::exists(imageFile)) {
        image = QImage(DEFAULT_SIZE, QImage::Format_ARGB32_Premultiplied);
        image.fill(color.isEmpty() ? DEFAULT_COLOR : color);
    } else {
        image = QImage(imageFile);
    }

    blurPixmap(image, saveToPixmap);
}

void PixmapProvider::blurPixmap(QImage &image, QPixmap &saveToPixmap)
{
    cv::Mat input = cv::Mat(image.height(), image.width(), CV_8UC4,
                            (void*)image.constBits(), image.bytesPerLine());
    cv::Mat output;
    //执行高斯模糊
    cv::GaussianBlur(input, output, cv::Size(101, 101), 0, 0);

    QImage outputImage(output.data, output.cols, output.rows,
                       static_cast<int>(output.step), QImage::Format_ARGB32_Premultiplied);

    QPixmap blurPixmap = QPixmap::fromImage(outputImage.copy());

    if (blurPixmap.isNull()) {
        blurPixmap = QPixmap::fromImage(image);
    }

    saveToPixmap.swap(blurPixmap);
}

QPixmap &PixmapProvider::getPixmap(BackgroundType::Type type)
{
    switch (type) {
        default:
        case BackgroundType::DesktopPic: {
            return m_desktopPixmap;
        }
        case BackgroundType::ScreensaverPic: {
            return m_screensaverPixmap;
        }
    }
}

const QString &PixmapProvider::pictureOption()
{
    return m_pictureOptions;
}

void PixmapProvider::loadPixmap(BackgroundType::Type type)
{
    switch (type) {
        default:
        case BackgroundType::DesktopPic: {
            loadDesktopBackground();
            break;
        }
        case BackgroundType::ScreensaverPic: {
            loadScreensaverPic();
            break;
        }
    }
}
