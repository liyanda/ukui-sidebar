INCLUDEPATH += $$PWD

HEADERS += \
        $$PWD/commons.h \
        $$PWD/theme-icon.h \
        $$PWD/pixmap-provider.h \
        $$PWD/desktop-background.h

SOURCES += \
        $$PWD/theme-icon.cpp \
        $$PWD/pixmap-provider.cpp \
        $$PWD/desktop-background.cpp
