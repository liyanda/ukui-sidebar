#include <QGuiApplication>
#include <QTime>
#include <QFile>
#include <QStandardPaths>

#include "side-bar-application.h"

void messageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    QByteArray currentTime = QTime::currentTime().toString().toLocal8Bit();

    bool showDebug = true;

    QString logFilePath = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/.log/ukui-sidebar.log";
    if (!QFile::exists(logFilePath)) {
        showDebug = false;
    }
    FILE *log_file = nullptr;

    if (showDebug) {
        log_file = fopen(logFilePath.toLocal8Bit().constData(), "a+");
    }

    const char *file = context.file ? context.file : "";
    const char *function = context.function ? context.function : "";
    switch (type) {
    case QtDebugMsg:
        if (!log_file) {
            break;
        }
        fprintf(log_file, "Debug: %s: %s (%s:%u, %s)\n", currentTime.constData(), localMsg.constData(), file, context.line, function);
        break;
    case QtInfoMsg:
        fprintf(log_file? log_file: stdout, "Info: %s: %s (%s:%u, %s)\n", currentTime.constData(), localMsg.constData(), file, context.line, function);
        break;
    case QtWarningMsg:
        fprintf(log_file? log_file: stderr, "Warning: %s: %s (%s:%u, %s)\n", currentTime.constData(), localMsg.constData(), file, context.line, function);
        break;
    case QtCriticalMsg:
        fprintf(log_file? log_file: stderr, "Critical: %s: %s (%s:%u, %s)\n", currentTime.constData(), localMsg.constData(), file, context.line, function);
        break;
    case QtFatalMsg:
        fprintf(log_file? log_file: stderr, "Fatal: %s: %s (%s:%u, %s)\n", currentTime.constData(), localMsg.constData(), file, context.line, function);
        break;
    }

    if (log_file)
        fclose(log_file);
}
int main(int argc, char *argv[])
{
    qInstallMessageHandler(messageOutput);
    SideBarApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    SideBarApplication::setApplicationName(QObject::tr("ukui-sidebar"));
    SideBarApplication::setApplicationVersion(VERSION);

    /* 如果系统中有实例在运行则退出 */
    QString id = QString("ukui-sidebar-qml-" + QLatin1String(getenv("DISPLAY")));
    SideBarApplication sideBarApplication(id, argc, argv);
    if (sideBarApplication.isRunning()) {
        //已经有实例在运行
        return 0;
    }

    return SideBarApplication::exec();
}
