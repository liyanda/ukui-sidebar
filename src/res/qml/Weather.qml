/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import QtQuick.Layouts 1.12
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0

StyleBackground {
    // 布局配置
    readonly property LayoutHelper weatherLayoutHelper: LayoutHelper {
        componentId: LayoutComponent.SidebarWeather;
    }

    RowLayout {
        anchors.fill: parent;
        anchors.leftMargin: weatherLayoutHelper.leftMargin;
        anchors.topMargin: weatherLayoutHelper.topMargin;
        anchors.rightMargin: weatherLayoutHelper.rightMargin;
        anchors.bottomMargin: weatherLayoutHelper.bottomMargin;

        spacing: weatherLayoutHelper.spacing;

        HighlightIcon {
            id: weatherIcon;

            Layout.preferredWidth: 16;
            Layout.preferredHeight: 16;
            Layout.alignment: Qt.AlignVCenter

            forceHighlight: true;
            iconName: weatherHelper.getIcon();
        }

        StyleText {
            id: weatherText
            Layout.fillWidth: true;
            Layout.fillHeight: true;
            verticalAlignment: Text.AlignVCenter
            text: weatherHelper.getWeather()
        }

        Component.onCompleted: {
            weatherHelper.weatherInfoChanged.connect(onWeatherInfoChanged);
        }

        function onWeatherInfoChanged(weather, icon) {
            weatherText.text = weather;
            weatherIcon.iconName = icon;
        }
    }

    MouseArea {
        anchors.fill: parent;
        onClicked: {
            weatherHelper.openWeather();
        }
    }
}
