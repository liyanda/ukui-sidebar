/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import QtQuick.Layouts 1.2
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0
import QtQuick.Controls 2.12

StyleBackground {
    id: root;

    property int index: 0;
    property string icon: "";
    property string toolTip: ""
    property int value: 0;

    // 布局配置
    readonly property LayoutHelper progressBarLayoutHelper: LayoutHelper {
        componentId: LayoutComponent.SidebarProgressBar;
    }

    signal sliderChanged (int value);

    radius: progressBarLayoutHelper.radius;

    function updateLayout() {
        buttonIcon.Layout.preferredWidth = layoutConfig.width(LayoutComponent.SidebarProgressBarIcon);
        buttonIcon.Layout.preferredHeight = layoutConfig.height(LayoutComponent.SidebarProgressBarIcon);

        controlBase.Layout.preferredHeight = layoutConfig.height(LayoutComponent.SidebarProgressBarSlider);
        sliderBackground.radius = layoutConfig.radius(LayoutComponent.SidebarProgressBarSlider);
    }

    Component.onCompleted: {
        updateLayout();
        layoutConfig.layoutChanged.connect(updateLayout);
    }

    Component.onDestruction: {
        layoutConfig.layoutChanged.disconnect(updateLayout);
    }

    RowLayout {
        id: layout;
        spacing: progressBarLayoutHelper.spacing;

        anchors.fill: parent;
        anchors.leftMargin: progressBarLayoutHelper.leftMargin;
//        anchors.topMargin: progressBarLayoutHelper.topMargin;
        anchors.rightMargin: progressBarLayoutHelper.rightMargin;
//        anchors.bottomMargin: progressBarLayoutHelper.bottomMargin;

        HighlightIcon {
            id: buttonIcon;

            forceHighlight: true;
            Layout.alignment: Qt.AlignVCenter;

            iconName: root.icon;
            MouseArea {
                property bool hovered: false;
                anchors.fill: parent
                hoverEnabled: true;
                ToolTip.visible: hovered;
                ToolTip.text: root.toolTip
                onEntered: {
                    hovered = true;
                }
                onExited: {
                    hovered = false;
                }
            }
        }

        Item {
            id: controlBase;

            Layout.fillWidth: true;
            Layout.alignment: Qt.AlignVCenter;

            Slider {
                id: control;
                anchors.fill: parent;
                value: root.value / 100;

                padding: 0;

                onPressedChanged: {
                    if (root.ListView.view) {
                        root.ListView.view.model.disableValueUpdate(pressed);
                    }
                }

                onValueChanged: {
                    if (pressed) {
                        root.sliderChanged(value * 100);
                    }
                }
                //进度条
                background: StyleBackground {
                    id: sliderBackground;
                    implicitWidth: controlBase.width;
                    implicitHeight: controlBase.height;

//                    radius: 16;
                    paletteRole: PaletteRole.Button;

                    StyleBackground {
                        id: top;
                        width: control.visualPosition * (control.availableWidth - (radius * 2 + whiteSpot.width)) + (radius * 2 + whiteSpot.width);
                        height: parent.height;
                        radius: sliderBackground.radius;
                        useStyleTransparency: false;
                        paletteRole: PaletteRole.Highlight;

                        Rectangle {
                            id: whiteSpot;
                            width: top.radius / 2;
                            height: top.radius;
                            color: "white";
                            radius: 5;

                            anchors.right: top.right;
                            anchors.rightMargin: top.radius;
                            anchors.verticalCenter: top.verticalCenter;
                        }
                    }
                }
                //滑块
                handle: Item {
                    id: sliderHandel;
                    x: control.visualPosition * (control.availableWidth - width);
                    y: control.availableHeight / 2 - height / 2;
                    implicitWidth: controlBase.height;
                    implicitHeight: controlBase.height;
                }
            }
        }
    }
}
