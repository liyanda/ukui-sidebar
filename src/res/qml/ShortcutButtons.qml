/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import QtQuick.Layouts 1.12
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0
import org.ukui.sidebar.shortcut.core 1.0

Item {
    id: root
    height: shortcutLayout.height
    state: ""

    property bool isFold: false
    property int  radius: layoutConfig.radius(LayoutComponent.SidebarFoldShortcutArea)
    property int foldY: 0
    property int unFoldY: 0

    onUnFoldYChanged: {
        foldY = Math.floor(unFoldY + weather.height + progressBarView.height + iconButtonView.height/2 + 2);
    }

    states: [
        State {
            name: "fold"
            PropertyChanges { target: root; y: root.foldY }
            PropertyChanges { target: iconButton; iconName: "ukui-sidebar-open-symbolic" }
        },
        State {
            name: "unFold"
            PropertyChanges { target: root; y: root.unFoldY }
            PropertyChanges { target: iconButton; iconName: "ukui-sidebar-fold-symbolic" }
        }
    ]

    transitions: Transition {
        NumberAnimation { properties: "y"; easing.type: Easing.InOutQuad; duration: 200 }
    }

    function changeState() {
        state = (state === "fold" ? "unFold" : "fold");
    }

    function updateLayout() {
        radius = layoutConfig.radius(LayoutComponent.SidebarFoldShortcutArea);
        shortcutLayout.spacing = layoutConfig.spacing(LayoutComponent.SidebarMain);
        foldButton.Layout.preferredHeight = layoutConfig.height(LayoutComponent.SidebarFoldShortcutArea);
        userInfo.Layout.preferredHeight = layoutConfig.height(LayoutComponent.SidebarUserInfo);

        let isTabletMode = layoutConfig.isTabletMode();
        separatorLineA.visible = !isTabletMode;
        separatorLineB.visible = !isTabletMode;
        separatorLineC.visible = !isTabletMode;

        state = "unFold";
    }

    Component.onCompleted: {
        updateLayout();
        layoutConfig.layoutChanged.connect(updateLayout);
    }

    Component.onDestruction: {
        layoutConfig.layoutChanged.disconnect(updateLayout);
    }

    ColumnLayout {
        id: shortcutLayout
        width: parent.width

        //折叠功能区域
        StyleBackground {
            id: foldButton
            Layout.fillWidth: true

            Item {
                anchors.centerIn: parent
                height: parent.height
                width: height

                //折叠按钮
                HighlightIcon {
                    id: iconButton
                    width: parent.width
                    height: width
                    opacity: 0.20
                    anchors.centerIn: parent
                    iconName: "ukui-sidebar-fold-symbolic"
                    forceHighlight: true
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        root.changeState();
                    }
                }
            }
        }

        UserInfo {
            id: userInfo
            Layout.fillWidth: true
        }

        Separator {
            id: separatorLineA
            Layout.fillWidth: true
            Layout.preferredHeight: height
        }

        MenuButtonView {
            id: menuButtonView
            Layout.fillWidth: true
            Layout.preferredHeight: height
        }

        IconButtonView {
            id: iconButtonView
            Layout.fillWidth: true
            Layout.preferredHeight: layoutHelper.height
        }

        Separator {
            id: separatorLineB
            Layout.fillWidth: true
            Layout.preferredHeight: height
        }

        ProgressBarView {
            id: progressBarView
            Layout.fillWidth: true
            Layout.preferredHeight: contentHeight
        }

        Separator {
            id: separatorLineC
            Layout.fillWidth: true
            Layout.preferredHeight: height
        }

        Weather {
            id: weather
            Layout.fillWidth: true
            Layout.preferredHeight: weatherLayoutHelper.height
        }
    }
}
