/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0

//PC侧边栏顶部显示
Item {
    id: sidebarTips;
    signal clearClick();

    StyleText {
        anchors.verticalCenter: parent.verticalCenter;
        font.pixelSize: 16;
        verticalAlignment: Text.AlignVCenter;
        font.bold: true;
        text: qsTr("Notification Center");
        paletteRole: PaletteRole.ButtonText;
    }

    Rectangle {
        id: sidebarAllClear;
        height: 36;
        width: 76;
        radius: 6;
        anchors.right: sidebarSystemSetting.left;
        anchors.verticalCenter: parent.verticalCenter;
        color: Qt.rgba(0,0,0,0);
        StyleText {
            anchors.centerIn: parent;
            font.pixelSize: 14;
            text: qsTr("Clear All");
        }

        MouseArea {
            anchors.fill: parent;
            hoverEnabled: true;
            onEntered: {
                sidebarAllClear.color =  Qt.rgba(0,0,0,0.1);
            }

            onPressed: {
                sidebarAllClear.color =  Qt.rgba(0,0,0,0.16);
            }

            onReleased: {
                sidebarAllClear.color =  Qt.rgba(0,0,0,0);
            }

            onClicked: {
                sidebarTips.clearClick();
            }

            onExited: {
                sidebarAllClear.color =  Qt.rgba(0,0,0,0);
            }
            onCanceled: {
                sidebarAllClear.color =  Qt.rgba(0,0,0,0);
            }
        }
    }

    Rectangle {
        id: sidebarSystemSetting;
        height: 36;
        width: 76;
        radius: 6;
        color: Qt.rgba(0,0,0,0);

        anchors.right: parent.right;
        anchors.verticalCenter: parent.verticalCenter;

        StyleText {
            anchors.centerIn: parent;
            font.pixelSize: 14;
            text: qsTr("Setting");
        }

        MouseArea {
            anchors.fill: parent;
            hoverEnabled: true;
            onEntered: {
                sidebarSystemSetting.color =  Qt.rgba(0,0,0,0.1);
            }

            onPressed: {
                sidebarSystemSetting.color =  Qt.rgba(0,0,0,0.16);
            }

            onReleased: {
                sidebarSystemSetting.color =  Qt.rgba(0,0,0,0);
            }

            onClicked: {
                notificationManager.openSystemSetting();
            }

            onExited: {
                sidebarSystemSetting.color =  Qt.rgba(0,0,0,0);
            }
            onCanceled: {
                sidebarSystemSetting.color =  Qt.rgba(0,0,0,0);
            }
        }
    }
}
