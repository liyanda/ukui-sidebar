/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

// 执行动画
function doAnimation(id, from, to, duration) {
    id.from = from
    id.to = to
    id.duration = duration < 50 ? 50 : duration
    id.start()
}

// 跟随侧边栏手势拖出侧边栏
function trackMouse(posX) {
    if (contentVisible) {
        return;
    }

    // 鼠标位置与当前主屏幕右边界的距离
    let distanceX = sidebarWindow.primaryScreenRight - posX;
    if (distanceX < 0) {
        if (sidebarWindow.visible) {
            hideItems();
        }
        return;
    }

    // 加上padding忽略窗口的边距
    let shortcutWidth = layoutConfig.width(LayoutComponent.SidebarMain) + sidebarWindow.windowPadding;
//        console.log("trackMouse:", posX, distanceX, shortcutWidth);
    if (distanceX > shortcutWidth) {
        distanceX = shortcutWidth;
    }

    if (!sidebarWindow.visible) {
        showItems();
    }

    shortcutArea.x = shortcutWidth - distanceX;
}

// 唤起侧边栏手势释放事件
function mouseReleased(posX) {
    if (contentVisible) {
        return;
    }

    // 鼠标位置与当前主屏幕右边界的距离
    let distanceX = sidebarWindow.primaryScreenRight - posX;
    let shortcutWidth = layoutConfig.width(LayoutComponent.SidebarMain);
    let duration = standardDuration;

    // 如果为负值，快速隐藏
    if (distanceX < 0) {
        doAnimation(hideShortcutArea, shortcutArea.x, shortcutWidth+ sidebarWindow.windowPadding, duration);
        return
    }

    // 屏幕宽度的5%
    if (distanceX <= sidebarWindow.minimumThreshold) {
        duration = Math.floor(standardDuration * (distanceX / shortcutWidth));
//            console.log("mouseReleased rebound:", duration, duration, (distanceX / shortcutWidth));
        doAnimation(hideShortcutArea, shortcutArea.x, shortcutWidth + sidebarWindow.windowPadding, duration);
        return;
    }

    // 确保计算的distanceX值始终小于等于shortcutWidth，防止出现负的时间
    if (distanceX > shortcutWidth) {
        distanceX = shortcutWidth;
    }

    duration = Math.floor(standardDuration * ((shortcutWidth - distanceX) / shortcutWidth));
    console.log("mouseReleased showShortcutArea:", duration, (shortcutWidth - distanceX), shortcutWidth);
    doAnimation(showShortcutArea, shortcutArea.x, 0, duration);
}

// 1.激活侧边栏
function activeSidebar() {
    console.log("activeSidebar:", contentVisible, sidebarWindow.x, sidebarWindow.width);
    if (contentVisible) {
        hideSidebarOnly();
    } else {
        showSidebarOnly();
    }
}

// 显示侧边栏
function showSidebarOnly() {
    console.log("showSidebarOnly:", contentVisible, showShortcutArea.running, hideShortcutArea.running)
    if (contentVisible || showShortcutArea.running || hideShortcutArea.running) {
        return;
    }

    showItems();
    doAnimation(showShortcutArea, (layoutConfig.width(LayoutComponent.SidebarMain) + sidebarWindow.windowPadding), 0, standardDuration);
}

// 隐藏侧边栏
function hideSidebarOnly() {
    console.log("hideSidebarOnly:", !contentVisible, showShortcutArea.running, hideShortcutArea.running)
    if (!contentVisible || showShortcutArea.running || hideShortcutArea.running) {
        return;
    }

    let shortcutAreaWidth = layoutConfig.width(LayoutComponent.SidebarMain);
    doAnimation(hideShortcutArea, shortcutArea.x, shortcutAreaWidth + sidebarWindow.windowPadding, standardDuration);
}

// 快速隐藏侧边栏
function hideSidebarQuickly() {
    if (!contentVisible || showShortcutArea.running || hideShortcutArea.running) {
        return;
    }
    hideItems();
}

// 1.更新主窗口的坐标属性
function updateContentGeometry() {
    let contentGeometry = sidebarWindow.sidebarContentGeometry;
    shortcutAreaBase.x = contentGeometry.x;
    shortcutAreaBase.y = contentGeometry.y;
    shortcutAreaBase.width = contentGeometry.width;
    shortcutAreaBase.height = contentGeometry.height;

    if (sidebarWindow.isTabletMode) {
        // 全屏模糊
        // TODO 监听该信号直接更新模糊区域
        let sidebarWindowRect = sidebarWindow.sidebarWindowGeometry;
        windowBlurHelper.setRegion(0, 0, sidebarWindowRect.width, sidebarWindowRect.height, 0);
    } else {
        windowBlurHelper.setRegion(shortcutAreaBase.x, shortcutAreaBase.y, shortcutAreaBase.width, shortcutAreaBase.height, sidebarWindow.radius);
    }
}

function showBlurArea() {
    if (!sidebarWindow.isTabletMode) {
        windowBlurHelper.radius = 0;
        windowBlurHelper.enable = true;
        windowBlurHelper.setRegion(shortcutAreaBase.x, shortcutAreaBase.y, shortcutAreaBase.width, shortcutAreaBase.height, sidebarWindow.radius);
        doAnimation(showGroundGlass, 0, 4000, 100);
    }
}

function showItems() {
    windowBlurHelper.radius = 0;
    sidebarWindow.visible = true;
    updateContentGeometry();
    shortcutAreaBase.visible = true;
    shortcutArea.x = layoutConfig.width(LayoutComponent.SidebarMain) + sidebarWindow.windowPadding;
    shortcutArea.visible = true;
}

function hideItems() {
    contentVisible = false;
    shortcutArea.visible = false;
    shortcutAreaBase.visible = false;
    sidebarWindow.visible = false;
}
