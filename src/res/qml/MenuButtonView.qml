/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0
import org.ukui.sidebar.shortcut.core 1.0

GridView {
    id: gridView

    readonly property LayoutHelper layoutHelper: LayoutHelper {
        componentId: LayoutComponent.SidebarMenuButtonView
    }
    height: layoutHelper.height

    interactive: false
    // TODO 数量为奇数时，需要处理最后一个插件的宽度
    cellWidth: count > 1 ? Math.floor(width / 2) : width;
    cellHeight: layoutHelper.height
    // 数据为0时，不显示该界面
    visible: (count > 0 && height > 0)
    property int layoutSpacing: count > 1 ? layoutHelper.spacing : 0;

    model: modelManager.getModel(ShortcutPlugin.MenuButton)
    delegate: Item {
        width: gridView.cellWidth
        height: gridView.cellHeight

        MenuButton {
            anchors.fill: parent

            index: model.index
            icon: model.icon
            name: model.name
            color: model.color

            anchors.leftMargin: index % 2 === 0 ? 0 : Math.floor(gridView.layoutSpacing / 2)
            anchors.rightMargin: index % 2 === 0 ? Math.floor(gridView.layoutSpacing / 2) : 0

            onClicked: function (index) {
                gridView.model.active(index, ShortcutPlugin.Click)
            }

            onLongClicked: function (index) {
                gridView.model.active(index, ShortcutPlugin.LongClick)
            }

            onMenuRequest: function (index) {
                gridView.model.active(index, ShortcutPlugin.MenuRequest)
            }
        }
    }
}
