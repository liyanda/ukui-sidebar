/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.12
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0
import org.ukui.sidebar.notify.core 1.0

NotificationCenterWindow {
    id: root;
    title: "ukui-sidebar-notification-center";
    visible: false;

    Component.onCompleted: {
        handGestureHelper.notificationCenterCalled.connect(notificationCenterCalled);
        handGestureHelper.top2BottomReleased.connect(mouseReased);
        notificationEventBridge.requestHideNotification.connect(notificationHide);
        notificationEventBridge.requestShowNotification.connect(notificationShow);
        // 切换模式
        layoutConfig.systemModeChanged.connect(notificationHide);
    }

    function notificationCenterCalled(posY) {
        if (!root.visible) {
            root.visible = true;
        }

        if(!notificationBody.showFinished && !bodyHide.running) {
            notificationBody.visible = true;
            notificationBody.y = -(root.height - posY);
            bottomIconBase.state ="Open";
        }
    }

    function mouseReased(posX,posY) {
        if(notificationBody.showFinished) {
            return;
        }

        if(notificationBody.y < -root.height * 0.75) {
            bodyHide.start();
            bottomIconBase.state ="Close";
        } else {
            notificationBody.showFinished = true;
            bodyShow.start();
        }
    }

    function notificationHide() {
        bodyHide.start();
        bottomIconBase.state ="Close";
    }

    function notificationShow() {
        if (!root.visible) {
            root.visible = true;
        }

        if(!notificationBody.showFinished) {
            notificationBody.visible = true;
            notificationBody.showFinished = true;
            bottomIconBase.state ="Open";
            bodyShow.start();
        }
    }

    Item {
        parent: root.contentItem;
        anchors.fill: parent;

        MouseArea {
            id: baseMouseArea;
            property int pressedPosY: 0;
            anchors.fill: parent;
            visible: !bodyHide.running && !bodyShow.running;

            onPressed: function pressed(event) {
                if (scrollableArea.interactive) {
                    pressedPosY = event.y;
                } else {
                    pressedPosY = scrollableArea.dragStartY;
                }
            }

            onPositionChanged: function postionChanged(event) {
                notificationBody.y = event.y - pressedPosY;
                bottomIconBase.state ="Close";
                if (notificationBody.y > 0) {
                    notificationBody.y =  0;
                }
            }

            onReleased: function release(event) {
                scrollableArea.interactive = true;
                // Icon鼠标事件press传递到base -> 判断是点击图标收起（通过Icon状态判断鼠标位置，运行收起动画 -> 相当于“Icon鼠标区域点击事件”），还是拖拽上移
                if (Math.abs(event.y -pressedPosY) < 1 &&  bottomIconBase.opacity === 0.65) {
                    bodyHide.start();
                } else {
                    if (notificationBody.y  < -root.height * 0.25) {
                        bodyHide.start();
                    } else if (notificationBody.y === 0) {
                        return;
                    } else {
                        bodyShow.start();
                        bottomIconBase.state ="Open";
                    }
                }
            }
        }

        Item {
            id: notificationBody
            width: parent.width;
            height: parent.height;
            visible: false;
            x: 0;
            y: -screenMonitor.getGeometry().height;
            focus: true;
            property int maxHeight: parent.height - bottomIconBase.height;
            property bool showFinished: false;

            onShowFinishedChanged: {
                notificationEventBridge.clearNotificationCenterRedPoint(showFinished)
            }

            PropertyAnimation {
                id: bodyShow;
                target: notificationBody;
                property: "y";
                easing.type: Easing.InOutSine
                to: 0;
                duration: 300;
                onFinished: {
                    bottomIconBase.state ="Close";
                }
            }

            PropertyAnimation {
                id: bodyHide;
                target: notificationBody;
                property: "y";
                to: -root.height
                easing.type: Easing.InOutSine

                duration: 300
                onFinished: {
                    notificationBody.visible = false;
                    root.visible = false;
                    notificationBody.showFinished = false;
                }
            }

            DesktopBackground {
                anchors.fill: parent;
                useDesktopBackground: false;
                z: 0;
            }

            Flickable {
                id: scrollableArea;
                width: 712;
                height: notificationBody.maxHeight;
                contentWidth: width
                contentHeight: timeInfo.height + notificationLayout.contentHeight;
                anchors.horizontalCenter: parent.horizontalCenter;
                clip: true;
                property int dragStartY: 0;
                property int viewHeightMin: scrollableArea.height - timeInfo.height;

                layer.enabled: isOpenGLEnv;
                layer.effect: OpacityMask {
                    maskSource: Rectangle {
                        width: scrollableArea.width;
                        height: scrollableArea.height;
                        radius: 24;
                    }
                }

                NotificationCenterHeader {
                    id: timeInfo;
                    width: parent.width;
                    height: 330;
                    itemCount: notificationLayout.count;
                    onClearClick: {
                        listViewClear.start();
                    }
                }

                MessageBoxGroup {
                    id: notificationLayout;
                    width: parent.width;
                    height: contentHeight < scrollableArea.viewHeightMin ? scrollableArea.viewHeightMin : contentHeight;
                    interactive: false;
                    anchors.top: timeInfo.bottom;

                    ParallelAnimation {
                        id: listViewClear;
                        NumberAnimation {
                            target: notificationLayout
                            property: "contentY"
                            to: notificationLayout.height
                            duration: 300
                            easing.type: Easing.InOutQuad
                        }

                        NumberAnimation {
                            target: notificationLayout
                            property: "opacity"
                            to: 0
                            duration: 300
                            easing.type: Easing.InOutQuad
                        }
                        onFinished: {
                            notificationLayout.allMessagesClear();
                            notificationManager.deleteAllMessage();
                            notificationLayout.opacity = 1.0;
                        }
                    }
                }
                MouseArea {
                    width: 712;
                    height: notificationBody.maxHeight;
                    z:1;
                    onPressed: {
                        if (mouse.y > scrollableArea.contentHeight) {
                            scrollableArea.interactive = false;
                            scrollableArea.dragStartY = mouse.y;
                        }
                        mouse.accepted = false;
                    }
                }
            }

            Item {
                id: bottomIconBase;
                height: 48;
                width: 48;
                anchors.bottom: notificationBody.bottom;
                anchors.horizontalCenter: notificationBody.horizontalCenter;
                state: "Open";
                opacity: 0.45;

                states: [
                    State {
                        name: "Open";
                        PropertyChanges {
                            target: bottomIcon;
                            source: "ukui-sidebar-fold-symbolic";
                        }
                    },
                    State {
                        name: "Close";
                        PropertyChanges {
                            target: bottomIcon;
                            source: "ukui-sidebar-open-symbolic";
                        }
                    }
                ]

                ThemeIcon {
                    id: bottomIcon;
                    height: 48;
                    width: 48;
                    anchors.centerIn: parent;
                    highLight: true;
                    source: "ukui-sidebar-fold-symbolic";
                }

                MouseArea {
                    id: iconMouseArea;
                    anchors.fill: parent;
                    visible: !bodyHide.running && !bodyShow.running;
                    hoverEnabled: true;

                    onEntered: {
                        bottomIconBase.opacity = 1.0;
                    }
                    onPressed: {
                        bottomIconBase.opacity = 0.65;
                        mouse.accepted = false;
                    }
                    onReleased: {
                        bottomIconBase.opacity = 0.45;
                    }
                    onExited: {
                        bottomIconBase.opacity = 0.45;
                    }
                    onCanceled: {
                        bottomIconBase.opacity = 0.45;
                    }
                }
            }
        }
    }
}
