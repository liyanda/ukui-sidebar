/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0
import QtGraphicalEffects 1.0

Item {
    id: messageFold;
    visible: false
    scale: 0;
    opacity: 0;

    property int layoutCount: 0;
    //单个折叠框高度
    property int fakeHeight: isNotificationCenter ? 140 : 136;
    //折叠框上下错开间距
    property int spacing: 8;
    //折叠框圆角
    property int fakeRadius: isNotificationCenter ? 24 : layoutConfig.radius(LayoutComponent.NotificationItem);

    onVisibleChanged: {
        if(messageFold.visible) {
            animation.target = messageFold;
            animation.start();
        }
    }

    onLayoutCountChanged: {
        if(messageFold.layoutCount === 3) {
            animation.target = messageFold2;
            animation.start();
        }
    }

    NumberAnimation {
        id: animation;
        easing.type: Easing.InOutCubic
        properties: "scale,opacity";
        from: 0;
        to: 1;
        duration: 150;
    }

    StyleBackground {
        id: messageFold1;
        x: 0;
        y: spacing;
        height: fakeHeight;
        width: messageFold.width - 32;
        radius: fakeRadius;
        visible: layoutCount >= 2;
        z: 1
        useStyleTransparency: false;
        paletteRole: PaletteRole.Base
        paletteGroup: isOpenGLEnv ? PaletteRole.Inactive : PaletteRole.Active;
        alpha: isOpenGLEnv ? 0.85 : 1;

        anchors.horizontalCenter: parent.horizontalCenter;

        layer.enabled: isOpenGLEnv;
        layer.effect: OpacityMask {
            maskSource: Item {
                height: messageFold1.height;
                width: messageFold1.width;

                Rectangle {
                    height: spacing;
                    width: messageFold1.width;
                    anchors.bottom: parent.bottom;
                }
            }
        }
    }

    StyleBackground {
        id: messageFold2;
        x: 0;
        y: spacing *2;
        height: fakeHeight;
        width: messageFold1.width - 32;
        radius: fakeRadius;
        visible: layoutCount > 2;
        z: 0
        useStyleTransparency: false;
        paletteRole: PaletteRole.Base
        paletteGroup: isOpenGLEnv ? PaletteRole.Disabled : PaletteRole.Active;
        alpha: isOpenGLEnv ? 0.65 : 1;

        anchors.horizontalCenter: parent.horizontalCenter;

        layer.enabled: isOpenGLEnv;
        layer.effect: OpacityMask {
            maskSource: Item {
                height: messageFold2.height;
                width: messageFold2.width;

                Rectangle {
                    height: spacing;
                    width: messageFold2.width;
                    anchors.bottom: parent.bottom;
                }
            }
        }
    }
}
