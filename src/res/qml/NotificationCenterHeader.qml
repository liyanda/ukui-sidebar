/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0
import  QtQuick.Layouts 1.12

ColumnLayout {
    id: root;
    spacing: 48;
    property int itemCount: 0;
    signal clearClick();

    Clock {
        id: clock;

        Layout.preferredWidth: 220;
        Layout.alignment: Qt.AlignHCenter;
        Layout.topMargin: 96;
    }

    // 通知中心顶部显示
    Item {
        id: rencentMessages;
        // 在通知中心，这块区域高度为48+8=56，在侧边栏内高度为：16+24+16=56
        Layout.fillHeight: true;
        Layout.fillWidth: true;
        // “通知中心”按钮 平板模式有消息时显示
        Item {
            id: notificationText;
            height: 32;
            width: notificationTextBody.width;
            anchors.bottom: parent.bottom;
            anchors.bottomMargin: 14;
            visible: root.itemCount > 0;

            StyleText {
                id: notificationTextBody;
                anchors.verticalCenter: parent.verticalCenter;
                font.pixelSize: 32;
                verticalAlignment: Text.AlignVCenter;
                font.bold: true;
                text: qsTr("Notification Center");
                paletteRole: PaletteRole.HighlightedText;
            }
        }

        Item {
            id: noMessageText;
            height: 32;
            width: visible ? noMessageTextBody.width : 0;
            anchors.bottom: parent.bottom;
            // 字体顶部有间隙  12+2
            anchors.bottomMargin: 14;
            x: visible ? (parent.width - noMessageTextBody.width)/2:0;
            visible: root.itemCount === 0;
            clip: true;
            Behavior on width {
                NumberAnimation {
                    duration: 300;
                    easing.type: Easing.InOutSine;
                }
            }
            Behavior on x {
                NumberAnimation {
                    duration: 300;
                    easing.type: Easing.InOutSine;
                }
            }

            StyleText {
                id: noMessageTextBody;
                anchors.verticalCenter: parent.verticalCenter;
                anchors.left: parent.left;

                font.pixelSize: 32;
                font.bold: true;
                text: qsTr("No new notifications");
                paletteRole: PaletteRole.HighlightedText;
            }
        }

        ThemeIcon {
            height: 24;
            width: 24;
            anchors.verticalCenter: parent.verticalCenter;
            anchors.left: noMessageText.visible ? noMessageText.right : notificationText.right;
            highLight: true;
            forceHighlight: true;
            source: "ukui-end-symbolic";

            MouseArea {
                anchors.fill: parent;
                onClicked: {
                    notificationManager.openSystemSetting();
                    notificationEventBridge.notificationHide();
                }
            }
        }

        // “清空”按钮——平板模式有消息时显示
        StyleBackground {
            id: allClear;
            height: 48;
            width: 64;
            radius: height / 2;
            visible: root.itemCount;

            anchors.rightMargin: 0;
            anchors.right: parent.right;
            anchors.verticalCenter: parent.verticalCenter;

            paletteRole: PaletteRole.Base;
            useStyleTransparency: false;
            alpha: 0.15;

            StyleText {
                anchors.centerIn: parent;
                font.pixelSize: 16;
                text: qsTr("Clear All");
                paletteRole: PaletteRole.HighlightedText;
            }

            MouseArea {
                anchors.fill: parent;
                hoverEnabled: true;

                onClicked: {
                    root.clearClick();
                }
                onEntered: {
                    allClear.alpha = 0.25;
                }
                onPressed: {
                    allClear.alpha = 0.10;
                }
                onReleased: {
                    allClear.alpha = 0.15;
                }
                onExited: {
                    allClear.alpha = 0.15;
                }
                onCanceled: {
                    allClear.alpha = 0.15;
                }
            }
        }

    }
}

