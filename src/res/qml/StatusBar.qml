/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import org.ukui.sidebar.core 1.0

StatusBarWindow {
    id: statusBar

    function systemModeChanged(mode) {
        visible = mode;
    }

    Component.onCompleted: {
        visible = layoutConfig.isTabletMode();
        layoutConfig.systemModeChanged.connect(systemModeChanged)
    }

    Item {
        parent: statusBar.contentItem;
        anchors.fill: parent;

        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton
            visible: statusBar.visible

            onPositionChanged: function(event) {
                if(event.y > statusBar.y) {
                    handGestureHelper.callNotificationCenter(event.y + statusBar.y)
                }
            }
            onReleased: function(event) {
                handGestureHelper.top2BottomRelease(event.x + statusBar.x, event.y + statusBar.y)
            }
        }
    }
}
