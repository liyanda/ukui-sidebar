/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import QtQuick.Layouts 1.12
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0

StyleBackground {
    id: root;

    property int index: 0;
    property string icon: "";
    property string name: "";
    property int color: 0;

    signal clicked(int index);
    signal longClicked(int index);
    signal menuRequest(int index);

    readonly property LayoutHelper menuButtonLayoutHelper: LayoutHelper {
        componentId: LayoutComponent.SidebarMenuButton;
    }

    radius: menuButtonLayoutHelper.radius;
    Component.onCompleted: {}

    RowLayout {
        id: layout;
        clip: true;
        spacing: menuButtonLayoutHelper.spacing;

        anchors.fill: parent;
        anchors.leftMargin: menuButtonLayoutHelper.leftMargin;
        anchors.topMargin: menuButtonLayoutHelper.topMargin;
        anchors.rightMargin: menuButtonLayoutHelper.rightMargin;
        anchors.bottomMargin: menuButtonLayoutHelper.bottomMargin;

        GeneralButton {
            id: buttonIcon;

            Layout.preferredWidth: 64;
            Layout.preferredHeight: width;
            Layout.alignment: Qt.AlignVCenter;

            radius: Math.floor(width / 2);

            iconName: root.icon;
            colorRole: root.color;

            onClicked: {
                root.clicked(root.index);
            }

            onLongClicked: {
                root.longClicked(root.index);
            }
        }

        Item {
            id: textArea
            Layout.fillWidth: true;
            Layout.fillHeight: true

            StyleText {
                id: buttonName;
                anchors.fill: parent
                text: root.name;
                elide: Text.ElideRight; //右侧超出省略
                verticalAlignment: Text.AlignVCenter;

                MouseArea {
                    anchors.fill: parent;

                    onClicked: {
                        root.menuRequest(root.index);
                    }
                }
            }
        }

        HighlightIcon {
            id: functionArea;
            Layout.preferredWidth: 24;
            Layout.preferredHeight: 24;
            Layout.alignment: Qt.AlignVCenter;

            iconName: "ukui-down-symbolic";

            MouseArea {
                anchors.fill: parent;

                onClicked: {
                    root.menuRequest(root.index);
                }
            }
        }
    }
}
