/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0

Item {
    id: root;

    property int index: 0;
    property string icon: "";
    property string name: "";
    property int color: 0;
    property bool disable: false;

    enabled: !disable;

    signal clicked(int index);
    signal longClicked(int index);

    function updateLayout() {
        layout.spacing = layoutConfig.spacing(LayoutComponent.SidebarIconButton);
        button.Layout.preferredWidth = layoutConfig.width(LayoutComponent.SidebarIconButtonIcon);
    }

    Component.onCompleted: {
        updateLayout();
        layoutConfig.layoutChanged.connect(updateLayout);
    }

    Component.onDestruction: {
        layoutConfig.layoutChanged.disconnect(updateLayout);
    }

    ColumnLayout {
        id: layout;

        anchors.fill: parent;
        clip: true;

        GeneralButton {
            id: button;

//            Layout.preferredWidth: 64;
            Layout.preferredHeight: width;
            Layout.alignment: Qt.AlignHCenter;

            radius: width / 2;

            disable: root.disable;
            iconName: root.icon;
            colorRole: root.color;

            onClicked:  {
                root.clicked(root.index);
            }
            onLongClicked: {
                root.longClicked(root.index);
            }
        }

        StyleText {
            id: buttonName;

            Layout.fillWidth: true;
            Layout.fillHeight: true;

            text: root.name;
            elide: Qt.ElideRight;

            horizontalAlignment: Text.AlignHCenter;
            verticalAlignment: Text.AlignTop;
        }



    }
}
