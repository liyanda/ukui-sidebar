/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12
import org.ukui.sidebar.notify.core 1.0
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0
import QtGraphicalEffects 1.0

ListView {
    id: messageBoxList;
    width: parent.width;
    height: contentHeight;
    focus: true;
    clip: true;
    spacing: isNotificationCenter ? 16 : 8;
    displayMarginBeginning: 1200
    displayMarginEnd: 1200
    cacheBuffer: 1200
    highlightFollowsCurrentItem: false;

    //接收信号
    function createItem(message) {
        //根据appName判断  新建 添加
        let item = messageBoxList.findItemByName(message.appName);

        if (item === null) {
            messageModel.insert(0,{appName: message.appName});
            item = messageBoxList.findItemByName(message.appName);
        }

        item.addMessage(message);
    }

    function syncMessage(id,appName) {
        let index = 0;
        let itemCount = messageBoxList.count;

        for (index; index < itemCount; ++index) {
            messageBoxList.currentIndex = index;

            if (messageBoxList.currentItem.appName === appName) {
                messageBoxList.currentItem.syncDeleteMessage(id);
            }
        }
    }

    Component.onCompleted: {
        notificationManager.newMessage.connect(createItem);
        notificationManager.syncDeleteAllMessage.connect(allMessagesClear);
        notificationManager.syncDeleteAppMessage.connect(syncAppMessage);
        notificationManager.syncDeleteMessage.connect(syncMessage);
        dateTimeUtils.timeRefresh();
    }

    function syncAppMessage(appName) {
        messageModel.removeItemByName(appName);
    }

    function allMessagesClear() {
        messageModel.clear();
    }

    function findItemByName(appName) {
        let index = 0;
        let itemCount = messageBoxList.count;

        //遍历item里appName
        for (index; index < itemCount; ++index) {
            messageBoxList.currentIndex = index;
            if (messageBoxList.currentItem.appName === appName) {
                return currentItem;
            }
        }
        return null;
    }

    model: ListModel {
        id: messageModel;
        function findIndexByName(appName) {
            let index = 0;
            let targetObj = null;
            //遍历item里appName
            for (index; index < count; ++index) {
                targetObj = get(index);
                messageBoxList.currentIndex = index;
                if (targetObj.appName === appName) {
                    return index;
                }
            }

            return -1;
        }

        function removeItemByName(appName) {
            let index = findIndexByName(appName);
            if (index !== -1) {
                remove(index);
            }
        }
    }

    delegate: MessageBox {
        id: messageBox;
        appName: model.appName;
        width: ListView.view ? ListView.view.width : 0;
        onMessageCleared: {
            messageModel.removeItemByName(appName);
            notificationManager.deleteAppMessage(appName);
        }
    }

    displaced: Transition {
        NumberAnimation {
            property: "y";
            easing.type: Easing.OutQuad;
            duration: 400;
        }
    }
}

