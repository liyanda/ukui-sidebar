/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.12
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0
import org.ukui.sidebar.shortcut.core 1.0

Rectangle {
    color: "transparent";

    readonly property LayoutHelper shortcutLayoutHelper: LayoutHelper {
        componentId: LayoutComponent.SidebarMain
    }

    anchors.leftMargin: shortcutLayoutHelper.leftMargin
    anchors.topMargin: shortcutLayoutHelper.topMargin
    anchors.rightMargin: shortcutLayoutHelper.rightMargin
    anchors.bottomMargin: shortcutLayoutHelper.bottomMargin

    function getShortcutAreaHeight() {
        return shortcutButtons.height
    }

    function updateColor() {
        if (layoutConfig.isTabletMode()) {
            color = "transparent";
        } else {
            color = themePalette.paletteColorWithTransparency(PaletteRole.Window, PaletteRole.Active);
        }
    }

    function systemModeChanged() {
        let isTabletMode = layoutConfig.isTabletMode();
        messageBox.visible = !isTabletMode;
        updateColor();
    }

    Component.onCompleted: {
        systemModeChanged();
        colorHelper.styleColorChanged.connect(updateColor)
        layoutConfig.systemModeChanged.connect(systemModeChanged);
    }

    Component.onDestruction: {
        colorHelper.styleColorChanged.disconnect(updateColor)
        layoutConfig.systemModeChanged.disconnect(systemModeChanged);
    }

    Item {
        width: parent.width;
        height: shortcutButtons.y >= 0 ? shortcutButtons.y : 0;

        NotificationCenterPC {
            id: messageBox;
            anchors.fill: parent;
            anchors.leftMargin: 16;
            anchors.rightMargin: 16;
        }
    }

    ShadowComponent {
        radius: shortcutButtons.radius;
        z: 5;
        x: shortcutButtons.x - shadowWidth;
        y: shortcutButtons.y - shadowWidth;
        width: shortcutButtons.width+shadowWidth*2;
        height: shortcutButtons.height+shadowWidth*2;

        visible: !sidebarWindow.isTabletMode && isOpenGLEnv;
    }

    ShortcutButtons {
        id: shortcutButtons;
        width: parent.width;
        y: parent.height - height;
        z: 10;
        unFoldY: parent.height - height;

        //遮罩圆角
        layer.enabled: isOpenGLEnv;
        layer.effect: OpacityMask {
            maskSource: Rectangle {
                width: shortcutButtons.width;
                height: shortcutButtons.height;
                radius: shortcutButtons.radius;
            }
        }
    }
}
