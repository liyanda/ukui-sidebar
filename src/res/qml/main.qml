﻿/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.12
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0
import org.ukui.sidebar.shortcut.core 1.0
import "sidebarWindow.js" as SidebarFunctions

SidebarWindow {
    id: sidebarWindow;
    visible: false;

    property int radius: 12;
    property int standardDuration: 300;

    onContentVisibleChanged: {
        sidebarEventBridge.clearSidebarRedPoint(contentVisible);
    }

    onRequestHideContent: {
        if (contentVisible) {
            showShortcutArea.stop()
            hideShortcutArea.stop()
        }
        SidebarFunctions.hideSidebarOnly();
    }

    onSidebarContentGeometryChanged: {
        SidebarFunctions.hideSidebarQuickly();
        SidebarFunctions.updateContentGeometry();
    }

    onIsTabletModeChanged: {
        SidebarFunctions.hideSidebarQuickly();
    }

    onActiveFocusItemChanged: {
        if (activeFocusItem === null) {
            if(contentVisible) {
                showShortcutArea.stop()
                hideShortcutArea.stop()
            }
            SidebarFunctions.hideSidebarOnly();
        }
    }

    WindowBlurHelper {
        id: windowBlurHelper;
        window: sidebarWindow;

        onRadiusChanged: {
            sidebarWindow.update()
        }
    }

    PropertyAnimation {
        id: hideShortcutArea;
        target: shortcutArea;
        property: "x";
        easing.type: Easing.OutQuart;
        onFinished: {
            SidebarFunctions.hideItems();
        }
    }

    PropertyAnimation {
        id: showShortcutArea;
        target: shortcutArea;
        property: "x";
        easing.type: Easing.OutQuart;
        onFinished: {
            contentVisible = true;
            SidebarFunctions.showBlurArea();
        }
    }

    PropertyAnimation {
        id: showGroundGlass;
        target: windowBlurHelper;
        property: "radius";
    }

    Component.onCompleted: {
        SidebarFunctions.updateContentGeometry();
        handGestureHelper.controlCenterCalled.connect(SidebarFunctions.trackMouse)
        handGestureHelper.right2LeftReleased.connect(SidebarFunctions.mouseReleased)

        sidebarEventBridge.requestActivateSidebar.connect(SidebarFunctions.activeSidebar)
        sidebarEventBridge.requestShowSidebar.connect(SidebarFunctions.showSidebarOnly)
        sidebarEventBridge.requestHideSidebar.connect(SidebarFunctions.hideSidebarOnly)
        sidebarEventBridge.requestHideQuickly.connect(SidebarFunctions.hideSidebarQuickly)
    }

    Item {
        parent: sidebarWindow.contentItem;
        anchors.fill: parent;

        MouseArea {
            anchors.fill: parent;
            property int startPosX: 0;
            property int shortcutAreaStartPosX: 0;
            property int shortcutAreaWidth: 0;
            onPressed: function(mouse) {
                // pc模式下拒绝接受事件
                if (!sidebarWindow.isTabletMode) {
//                    console.log("拒绝鼠标事件 pressed", mouse.x)
                    mouse.accepted = false;
                    return;
                }
                startPosX = mouse.x;
                shortcutAreaStartPosX = shortcutArea.x;
                shortcutAreaWidth = layoutConfig.width(LayoutComponent.SidebarMain) + sidebarWindow.windowPadding;
            }

            onReleased: function(mouse) {
                // 模仿点击事件
                if (Math.abs(mouse.x - startPosX) < 2) {
                    if ((mouse.x > shortcutAreaBase.x) && (mouse.x < (shortcutAreaBase.x + shortcutAreaBase.width))
                            && (mouse.y > (shortcutAreaBase.height - shortcutArea.getShortcutAreaHeight())) && (mouse.y < shortcutAreaBase.height)) {
                        return
                    }
                    if (sidebarWindow.contentVisible) {
                        SidebarFunctions.doAnimation(hideShortcutArea, shortcutArea.x, shortcutAreaWidth, standardDuration);
                    }
                    return;
                }

                // x 距离屏幕右侧的距离，0 <= shortcutArea.x <= shortcutAreaWidth
                let distanceX = shortcutAreaWidth - shortcutArea.x;
                let duration = standardDuration;

                // 剩余5%时，隐藏侧边栏,否则继续显示
                if (shortcutArea.x >= sidebarWindow.minimumThreshold) {
                    duration = Math.floor(standardDuration * (distanceX / shortcutAreaWidth));
                    SidebarFunctions.doAnimation(hideShortcutArea, shortcutArea.x, shortcutAreaWidth, duration);
                    return;
                }

                duration = Math.floor(standardDuration * (shortcutArea.x / shortcutAreaWidth));
                SidebarFunctions.doAnimation(showShortcutArea, shortcutArea.x, 0, duration);
                console.log("onReleased showShortcutArea:", duration, shortcutArea.x, shortcutAreaWidth);
            }

            onPositionChanged: function(mouse) {
                if (pressed) {
                    let distanceX = mouse.x - startPosX;
                    let newX = shortcutAreaStartPosX + distanceX;

                    // 边界值判断,确保：0 <= shortcutArea.x <= shortcutAreaWidth
                    if (newX < 0) {
                        newX = 0;

                    } else if (newX > shortcutAreaWidth) {
                        newX = shortcutAreaWidth;
                    }

                    shortcutArea.x = newX;
                }
            }
        }

        Rectangle {
            id: desktopBac;
            z: 0;
            anchors.fill: parent;
            visible: sidebarWindow.isTabletMode;
            onOpacityChanged: {
                update();
            }

            function updateColor() {
                color = colorHelper.mask();
            }
            Component.onCompleted: {
                updateColor();
                colorHelper.styleColorChanged.connect(updateColor);
            }

            Component.onDestruction: {
                colorHelper.styleColorChanged.disconnect(updateColor);
            }
        }

        // 阴影区域
        ShadowComponent {
            id: shadowArea;
            z: 5;
            visible: !sidebarWindow.isTabletMode && isOpenGLEnv;
            radius: sidebarWindow.radius;
            x: shortcutAreaBase.x - shadowWidth;
            y: shortcutAreaBase.y - shadowWidth;
            width: shortcutAreaBase.width + shadowWidth*2;
            height: shortcutAreaBase.height + shadowWidth*2;
        }

        Item {
            id: shortcutAreaBase;
            focus: true;
            z: 10;

            ShortcutArea {
                id: shortcutArea;
                visible: true
                width: shortcutLayoutHelper.width;
                height: parent.height;
                clip: true;
                layer.enabled: isOpenGLEnv;
                layer.effect: OpacityMask {
                    maskSource: Rectangle {
                        width: shortcutArea.width;
                        height: shortcutArea.height;
                        radius: sidebarWindow.radius;
                    }
                }

                onXChanged: {
                    shadowArea.x = shortcutAreaBase.x - shadowArea.shadowWidth + shortcutArea.x;

                    if (sidebarWindow.isTabletMode) {
                        let shortAreaWidth = width + sidebarWindow.windowPadding;
                        let rightWidth = Math.abs(Math.floor(shortAreaWidth - x));
                        windowBlurHelper.radius = Math.floor(4000 * (rightWidth / shortAreaWidth));
                        windowBlurHelper.enable = (rightWidth !== 0);
                        desktopBac.opacity = Math.abs(rightWidth / shortAreaWidth);
                    }
                    else {
                        windowBlurHelper.enable = (x === 0);
                    }
                }
            }
        }
    }
}
