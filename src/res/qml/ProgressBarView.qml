/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0
import org.ukui.sidebar.shortcut.core 1.0

ListView {
    id: listView;

    readonly property LayoutHelper layoutHelper: LayoutHelper {
        componentId: LayoutComponent.SidebarProgressBarView
    }

    height: contentHeight
    spacing: layoutHelper.spacing
    visible: (count > 0 && height > 0)
    interactive: false

    model: modelManager.getModel(ShortcutPlugin.ProgressBar)
    delegate: ProgressBar {
        width: listView.width
        // 通过布局配置设置高度
        height: progressBarLayoutHelper.height
        index: model.index
        icon: model.icon
        toolTip: model.tooltip
        value: model.value

        onSliderChanged: {
            listView.model.setValue(index, value)
        }
    }
}
