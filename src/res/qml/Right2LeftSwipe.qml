/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import org.ukui.sidebar.core 1.0

RightHandGestureWindow {
    id: right2LeftSwipe
    visible: true

    property int visualWidth: 2

    Item {
        parent: right2LeftSwipe.contentItem;
        anchors.fill: parent;

        MouseArea {
            property int distance
            property bool isBegin: false
            property int startX

            anchors.fill: parent
            acceptedButtons: Qt.LeftButton

            onPressed: {
                startX = mouseX
                distance = -1
                isBegin = false
            }
            onPositionChanged: function onPositionChanged(event) {
                if (pressed) {
                    distance = startX - event.x
                    if ((distance >= 12) || (isBegin)) {
                        isBegin = true
                        if ((event.x < right2LeftSwipe.visualWidth) && (distance >= 12)) {
                            handGestureHelper.callControlCenter(right2LeftSwipe.x + event.x + 12)
                        }
                        else {
                            handGestureHelper.callControlCenter(right2LeftSwipe.x + 1)
                        }
                    }
                }
            }
            onReleased: function releaseMouse(event) {
                if (isBegin) {
                    handGestureHelper.right2LeftRelease(event.x + right2LeftSwipe.x, event.y + right2LeftSwipe.y)
                }
            }
        }
    }
}
