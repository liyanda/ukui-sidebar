/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0
import org.ukui.sidebar.shortcut.core 1.0

StyleBackground {
    id: root;
    visible: (iconView.count > 0 && height > 0)

    readonly property LayoutHelper layoutHelper: LayoutHelper {
        componentId: LayoutComponent.SidebarIconButtonView
    }

    height: layoutHelper.height;
    radius: layoutHelper.radius;

    Item {
        id: viewContainer
        anchors.fill: parent
        anchors.topMargin: root.layoutHelper.topMargin
        anchors.bottomMargin: root.layoutHelper.bottomMargin

        GridView {
            id: iconView;
            anchors.fill: parent
            interactive: false
            cellWidth: Math.floor(width / 4)
            cellHeight: Math.floor(height / 2)

            model: modelManager.getModel(ShortcutPlugin.Icon)
            delegate: IconButton {
                width: iconView.cellWidth
                height: iconView.cellHeight
                disable: model.disabled
                index: model.index
                icon: model.icon
                name: model.name
                color: model.color

                onClicked: function (index) {
                    iconView.model.active(index, ShortcutPlugin.Click)
                }

                onLongClicked: function (index) {
                    iconView.model.active(index, ShortcutPlugin.LongClick)
                }
            }
        }
    }
}
