/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.12
import QtQuick.Layouts 1.12
import org.ukui.sidebar.notify.core 1.0
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0
import QtGraphicalEffects 1.0

Item {
    id: root;
    clip: true;

    Column {
        id: groupLayout;
        anchors.fill: parent;
        property int maxListHeight: groupLayout.height - messageHeaderTips.height;

        NotificationCenterLabel {
            id: messageHeaderTips;
            height: 56;
            width: parent.width;
            onClearClick: {
                listViewClear.start();
            }
        }

        MessageBoxGroup {
            id: messageBoxGroup;
            width: parent.width;
            height: groupLayout.maxListHeight;
            interactive: contentHeight > groupLayout.maxListHeight;
            ParallelAnimation {
                id: listViewClear;
                NumberAnimation {
                    target: messageBoxGroup
                    property: "contentY"
                    to: messageBoxGroup.height
                    duration: 300
                    easing.type: Easing.InOutQuad
                }

                NumberAnimation {
                    target: messageBoxGroup
                    property: "opacity"
                    to: 0
                    duration: 300
                    easing.type: Easing.InOutQuad
                }
                onFinished: {
                    messageBoxGroup.allMessagesClear();
                    notificationManager.deleteAllMessage();
                    messageBoxGroup.opacity = 1.0;
                }
            }
        }
    }

    StyleText {
        id: noMessage;
        font.pixelSize: 14;
        visible: messageBoxGroup.count === 0;
        anchors.centerIn: parent;
        text: qsTr("No new notifications received")
    }

}
