/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import org.ukui.sidebar.core 1.0
import org.ukui.sidebar.items 1.0

Item {
    id: root;

    // 决定是否对非纯色图标进行反色
    property bool forceHighlight: false;
    property string iconName: "";

    Component.onCompleted: {
        updateIcon();
        colorHelper.styleColorChanged.connect(updateIcon);
    }

    Component.onDestruction: {
        colorHelper.styleColorChanged.disconnect(updateIcon);
    }

    function updateIcon() {
        icon.highLight = colorHelper.isDarkStyle();
    }

    ThemeIcon {
        id: icon;

        anchors.fill: parent;
        anchors.centerIn: parent;

        forceHighlight: root.forceHighlight;
        source: root.iconName;
    }
}
