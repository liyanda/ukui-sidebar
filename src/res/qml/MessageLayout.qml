/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import org.ukui.sidebar.core 1.0

Item {
    id: root;

    // 布局间距
    property int itemHeight: 120;
    property int itemFoldHeight: 140;
    property int spacing: 8;

    property int count: children.length;
    property bool fold: false;

    function updateLayout() {
        itemHeight = layoutConfig.height(LayoutComponent.NotificationItem);
        // 在通知中心收起的高度是140，展开高度是120。在侧边栏的折叠高度为136
        itemFoldHeight = 136;
    }

    Component.onCompleted: {
        foldedMessage();
        if (!isNotificationCenter) {
            updateLayout();
            layoutConfig.layoutChanged.connect(updateLayout);
        }
    }

    Component.onDestruction: {
        layoutConfig.layoutChanged.disconnect(updateLayout);
    }

    onWidthChanged: {
        let index = 0;
        let reverseIndex = children.length -1;
        let child = null;
        for(;reverseIndex >= 0;--reverseIndex) {
            child = children[reverseIndex];
            // 侧边栏宽度384，左右间隔各16
            child.width = width;
            child.index = reverseIndex;
            ++index;
        }
    }

    function insertItem(item) {
        if (item === null) {
            return;
        }

        // 新添加的元素默认放在最前面
        item.x = 0;
        item.y = 0;
        item.width = root.width;
        item.height = itemHeight;

        // 将新元素添加到 children list中，之后会触发onChildrenChanged，
        // 然后会强制布局整个界面
        item.parent = root;
        //index 新添加的元素为最大值
        item.index = children.length - 1;
        // 更改元素的scale属性，用以触发缩放动画
        item.scale = 1;

        item.groupFold = fold;
        item.updateTips();
    }

    /**
     * 遍历子元素，将全部元素的位置正确排序
     */
    function forceLayout() {
        // index为item在视图中的位置索引
        let index = 0;
        let child = null;
        // 从最后一个元素开始遍历，将最后的元素放到界面的最前面。反转 （-1：重新排序 不需要添加）
        let reverseIndex = children.length - 1;
        for (; reverseIndex >= 0; --reverseIndex) {
            child = children[reverseIndex];
            child.height = itemHeight;
            child.visible = true;
            child.groupFold = false;
            // 使用children list中的索引，更新每个item的index
            child.index = reverseIndex;
            // 为item内部的属性动画设置目标值
            child.scale = 1;
            // 展开所有消息条alpha为0.85
            child.itemAlpha = 0.85;
            child.y = index * (itemHeight + spacing);
            ++index;
        }
        root.height = count * itemHeight + (count-1) * spacing;
    }

    function foldedMessage() {
        if (children.length <= 0) {
            return;
        }

        let index = 0;
        let child = null;
        // 从第三条开始进入for循环，缩放为0.9
        let reverseIndex = children.length - 3;

        // 折叠第一个item,折叠后顶部第一条消息透明度alpha为1.0，其余消息为0
        child = children[children.length - 1];
        child.height = itemFoldHeight;
        child.itemAlpha = 1.0;
        child.groupFold = true;
        child.reset();
        // 更新第一条索引
        child.index = reverseIndex + 2;
        child.updateTips();
        child.scale = 1.0;
        child.y = 0;

        // 第二个消息缩放0.95
        child = children[children.length - 2];
        child.groupFold = true;
        child.reset();
        // 更新第二条索引
        child.index = reverseIndex + 1;
        child.scale = 0.95;
        child.y = 30;

        for (; reverseIndex >= 0; --reverseIndex) {
            child = children[reverseIndex];
            child.reset();
            child.groupFold = true;
            // 使用children list中的索引，更新每个item的index
            child.index = reverseIndex;
            child.scale = 0.9;
            // 触发折叠动画
            child.y = 40;
            ++index;
        }
        root.height = itemFoldHeight;
    }

    // 同步删除单个消息
    function deleteMessage(id) {
        let index = 0;
        let child = null;

        for(;index <= children.length -1;++index) {
            child = children[index];
            if (child.id === id) {
                child.syncItemDelete();
                break;
            }
        }

        if (fold) {
            foldedMessage();
        } else {
            forceLayout();
        }
    }

    onChildrenChanged: {
        if (!fold) {
            forceLayout();
        }
    }

    onFoldChanged: {
        if (fold) {
            foldedMessage();
        } else {
            forceLayout();
        }
    }
}
