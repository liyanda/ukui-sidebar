# 使用的qt模块
QT += quick core KWindowSystem dbus x11extras
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

VERSION = 1.0.0
DEFINES += VERSION='\\"$${VERSION}\\"'


# 设置目标类型和目标名称
TEMPLATE = app
TARGET = ukui-sidebar

# 配置c++标准
CONFIG += c++11 release link_pkgconfig lrelease
QMAKE_CXXFLAGS += -Werror=return-type -Werror=return-local-addr -Werror=uninitialized -Werror=unused-label

# 导入gsetting相关依赖
PKGCONFIG += gsettings-qt kysdk-waylandhelper xcb

DEFINES += QT_DEPRECATED_WARNINGS
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += $$PWD/../ukui-shortcut

# 导入子目录
include($$PWD/config/config.pri)
include($$PWD/model/model.pri)
include($$PWD/3rd-parties/qtsingleapplication/qtsingleapplication.pri)
include($$PWD/items/items.pri)
include($$PWD/main/main.pri)
include($$PWD/notification/notification.pri)
include($$PWD/weather/weather.pri)

SOURCES += \
        $$PWD/main.cpp

RESOURCES += res/qml.qrc

# 链接库
LIBS += -L$$OUT_PWD/../ukui-shortcut -lukui-shortcut

target.path = /usr/bin
!isEmpty(target.path): INSTALLS += target

desktop_file.path = /etc/xdg/autostart
desktop_file.files += $$PWD/data/ukui-sidebar.desktop
INSTALLS += desktop_file

TRANSLATIONS += \
    $$PWD/translations/ukui-sidebar_zh_CN.ts

TRANSLATION_FILE_DIR = /usr/share/ukui-sidebar/translations
DEFINES += TRANSLATION_FILE_DIR='\\"$${TRANSLATION_FILE_DIR}\\"'

# 安装在编译过程中才会生成的文件需要指定 'no_check_exist'，否则不会在Makefile中生成对应的install规则
qm_files.CONFIG += no_check_exist
qm_files.path = $${TRANSLATION_FILE_DIR}

# 对每一个翻译文件生成对应的 make install 规则
for(file, TRANSLATIONS) {
    # 提取每一个.ts文件的文件名
    TS_FILE = $$basename(file)
    # 转换对应的.qm文件
    QM_FILE = $$replace(TS_FILE, ".ts", ".qm")

    qm_files.files += $${OUT_PWD}/.qm/$${QM_FILE}
    # message(QM_FILE = $${QM_FILE})
}

INSTALLS += qm_files
