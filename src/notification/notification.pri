INCLUDEPATH += $$PWD

include($$PWD/notification-helper/notification-helper.pri)
include($$PWD/datetime/date-time.pri)

HEADERS += \
    $$PWD/notification-manager.h

SOURCES += \
    $$PWD/notification-manager.cpp
