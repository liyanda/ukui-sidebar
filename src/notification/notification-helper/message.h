/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */
#ifndef MASSAGE_H
#define MASSAGE_H
#include <memory>
#include <QString>
#include <QObject>
#include <QDateTime>
#include <QMetaType>

#include "action.h"

namespace Notify {
class MessagePrivate;
class Message
{
    Q_GADGET
    Q_PROPERTY(QString id READ getID)
    Q_PROPERTY(QString appName READ getAppName WRITE setAppName)
    Q_PROPERTY(QString replacesID READ getReplacesID)
    Q_PROPERTY(QString notificationIcon READ getNotificationIcon WRITE setNotificationIcon)
    Q_PROPERTY(QString summary READ getSummary WRITE setSummary)
    Q_PROPERTY(QString body READ getBody WRITE setBody)
//    Q_PROPERTY(Action action READ getAction WRITE setAction)
    Q_PROPERTY(QDateTime timeStamp READ getTimeStamp WRITE setTimeStamp)

public:
    Message();
    explicit Message(const QString &replacesID);
    Message(const Message &rhs);
    ~Message();

    Message& operator =(const Message &rhs );

    QString getID() const;

    void setAppName(const QString &appName);
    QString getAppName() const;

    QString getReplacesID() const;

    void setNotificationIcon(const QString &notificationIcon);
    QString getNotificationIcon() const;

    void setSummary(const QString &summary);
    QString getSummary() const;

    void setBody(const QString &body);
    QString getBody() const;

    void setAction(const Action &action);
    Action getAction() const;

    void setTimeStamp(const QDateTime &timeStamp);
    QDateTime getTimeStamp() const;

    Q_INVOKABLE bool executeDefaultAction() const;

private:
    const std::unique_ptr<MessagePrivate> d;
};
}

#endif // MASSAGE_H
