INCLUDEPATH += $$PWD

SOURCES += \
    $$PWD/action.cpp \
    $$PWD/message.cpp \
    $$PWD/notification-helper.cpp \
    $$PWD/dbus-slots.cpp

HEADERS += \
    $$PWD/action.h \
    $$PWD/message.h \
    $$PWD/notification-helper.h \
    $$PWD/dbus-slots.h
