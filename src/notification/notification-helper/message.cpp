/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */
#include "message.h"
#include <QUuid>
using namespace Notify;

class Notify::MessagePrivate {
public:
    QString m_ID;
    QString m_appName;
    QString m_replaceID;
    QString m_notificationIcon;
    QString m_summary;
    QString m_body;
    Action m_action;
    QDateTime m_timeStamp;
};

Message::Message() : d(new MessagePrivate)
{
    d->m_ID = QUuid::createUuid().toString();
}

Message::Message(const QString &replacesID) : d(new MessagePrivate)
{
    if(replacesID.isEmpty()) {
        d->m_ID = QUuid::createUuid().toString();
    } else {
        d->m_ID = replacesID;
    }
}

Message::Message(const Message &rhs) : d(new MessagePrivate(*rhs.d))
{

}

Message::~Message()
{

}

Message &Message::operator =(const Message &rhs)
{
    *d = *rhs.d;
    return *this;
}

QString Message::getID() const
{
    return d->m_ID;
}

void Message::setAppName(const QString &appName)
{
    d->m_appName = appName;
}

QString Message::getAppName() const
{
    return d->m_appName;
}

QString Message::getReplacesID() const
{
    return d->m_replaceID;
}

void Message::setNotificationIcon(const QString &notificationIcon)
{
    d->m_notificationIcon = notificationIcon;
}

QString Message::getNotificationIcon() const
{
    return d->m_notificationIcon;
}

void Message::setSummary(const QString &summary)
{
    d->m_summary = summary;
}

QString Message::getSummary() const
{
    return d->m_summary;
}

void Message::setBody(const QString &body)
{
    d->m_body = body;
}

QString Message::getBody() const
{
    return d->m_body;
}

void Message::setAction(const Action &action)
{
    d->m_action = action;
}

Action Message::getAction() const
{
    return d->m_action;
}

void Message::setTimeStamp(const QDateTime &timeStamp)
{
    d->m_timeStamp = timeStamp;
}

QDateTime Message::getTimeStamp() const
{
    return d->m_timeStamp;
}

bool Message::executeDefaultAction() const
{
    return d->m_action.executeDefaultAction();
}
