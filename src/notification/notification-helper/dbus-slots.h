/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */
#ifndef DBUSSLOTS_H
#define DBUSSLOTS_H

#include <QObject>
#include "message.h"

#define DBUS_INTERFACE "org.ukui.Sidebar.notification"
namespace Notify {

class DbusSlots : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.ukui.Sidebar.notification")
public:
    explicit DbusSlots(QObject *parent = nullptr);
//临时接口
public Q_SLOTS:
    void sidebarNotification(const QString &appName, const QString &appIcon,
                             const QString &summary, const QString &body,
                             const QString &urlStr, const QString &action);
Q_SIGNALS:
    void newMessage(Message&);

signals:

};
}
#endif // DBUSSLOTS_H
