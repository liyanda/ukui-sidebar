/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */
#include "dbus-slots.h"
#include <QDBusConnection>
#include <QDateTime>
#include <QDebug>
#include "action.h"
using namespace Notify;
DbusSlots::DbusSlots(QObject *parent) : QObject(parent)
{
    QDBusConnection::sessionBus().unregisterService("org.ukui.Sidebar");
    QDBusConnection::sessionBus().registerService("org.ukui.Sidebar");
    QDBusConnection::sessionBus().registerObject("/org/ukui/Sidebar/notification", this, QDBusConnection::ExportAllSlots);
}

void DbusSlots::sidebarNotification(const QString &appName, const QString &appIcon, const QString &summary, const QString &body, const QString &urlStr, const QString &action)
{
    Message message;
    message.setAppName(appName);
    message.setNotificationIcon(appIcon);
    message.setSummary(summary);
    message.setBody(body);
    Action ac;
    if(!urlStr.isEmpty()) {
        ac.setUrl(urlStr);
    }
    if(!action.isEmpty()) {
        ac.addAction(action, "");
    }
    message.setAction(ac);
    message.setTimeStamp(QDateTime::currentDateTime());

    Q_EMIT newMessage(message);
}
