/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */
#ifndef ACTION_H
#define ACTION_H
#include <memory>
#include <QString>
#include <QMap>
namespace Notify {
class ActionPrivate;
class Action
{
public:
    Action();
    Action(const Action &other);
    ~Action();

    Action& operator =(const Action &other);

    void setUrl(const QString &url);
    QString getUrl() const;

    void addAction(const QString &action, const QString &displayName);
    /**
     * @brief executeDefaultAction
     * @return false means there is no default action to execute;
     */
    bool executeDefaultAction();

private:
    const std::unique_ptr<ActionPrivate> d;
};
}
#endif // ACTION_H
