<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MessageBox</name>
    <message>
        <location filename="../res/qml/MessageBox.qml" line="255"/>
        <source>Show less</source>
        <translation>折叠</translation>
    </message>
</context>
<context>
    <name>MessageBoxGroup</name>
    <message>
        <source>Rencent messages</source>
        <translation type="vanished">最近消息</translation>
    </message>
    <message>
        <source>Notification Center</source>
        <translation type="vanished">通知中心</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation type="vanished">没有新通知</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="vanished">清空</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>No new notifications received</source>
        <translation type="vanished">没有收到新通知</translation>
    </message>
</context>
<context>
    <name>MessageHeaderTips</name>
    <message>
        <source>Notification Center</source>
        <translation type="vanished">通知中心</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation type="vanished">没有新通知</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="vanished">清空</translation>
    </message>
    <message>
        <source>Recent Messages</source>
        <translation type="vanished">最近消息</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation type="vanished">设置</translation>
    </message>
</context>
<context>
    <name>MessageItem</name>
    <message>
        <location filename="../res/qml/MessageItem.qml" line="37"/>
        <location filename="../res/qml/MessageItem.qml" line="76"/>
        <source>%1 more message</source>
        <translation>还有%1则通知</translation>
    </message>
    <message>
        <location filename="../res/qml/MessageItem.qml" line="73"/>
        <source>%1 more messages</source>
        <translation>还有%1则通知</translation>
    </message>
    <message>
        <source>%1 more message%2</source>
        <translation type="vanished">还有%1则通知</translation>
    </message>
</context>
<context>
    <name>NotificationCenterHeader</name>
    <message>
        <location filename="../res/qml/NotificationCenterHeader.qml" line="59"/>
        <source>Notification Center</source>
        <translation>通知中心</translation>
    </message>
    <message>
        <location filename="../res/qml/NotificationCenterHeader.qml" line="94"/>
        <source>No new notifications</source>
        <translation>没有新通知</translation>
    </message>
    <message>
        <location filename="../res/qml/NotificationCenterHeader.qml" line="136"/>
        <source>Clear All</source>
        <translation>清空</translation>
    </message>
</context>
<context>
    <name>NotificationCenterLabel</name>
    <message>
        <location filename="../res/qml/NotificationCenterLabel.qml" line="33"/>
        <source>Notification Center</source>
        <translation>通知中心</translation>
    </message>
    <message>
        <location filename="../res/qml/NotificationCenterLabel.qml" line="48"/>
        <source>Clear All</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="../res/qml/NotificationCenterLabel.qml" line="92"/>
        <source>Setting</source>
        <translation>设置</translation>
    </message>
</context>
<context>
    <name>NotificationCenterPC</name>
    <message>
        <location filename="../res/qml/NotificationCenterPC.qml" line="58"/>
        <source>No new notifications received</source>
        <translation>没有收到新通知</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main/account-information.cpp" line="46"/>
        <source>administrators</source>
        <translation>管理员</translation>
    </message>
    <message>
        <location filename="../main/account-information.cpp" line="47"/>
        <source>standard users</source>
        <translation>普通用户</translation>
    </message>
    <message>
        <location filename="../main/side-bar-application.cpp" line="232"/>
        <source>Show the current state of the sidebar.</source>
        <translation>显示侧边栏现在的状态</translation>
    </message>
    <message>
        <location filename="../main/side-bar-application.cpp" line="233"/>
        <source>There are two options, &apos;notify&apos; and &apos;control&apos;.</source>
        <translation>notify -&gt;打开通知中心，control -&gt; 打开控制中心</translation>
    </message>
    <message>
        <location filename="../main/side-bar-application.cpp" line="234"/>
        <source>Quit sidebar.</source>
        <translation>退出侧边栏</translation>
    </message>
    <message>
        <location filename="../main/side-bar-application.cpp" line="333"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="55"/>
        <source>ukui-sidebar</source>
        <translation>侧边栏</translation>
    </message>
</context>
<context>
    <name>SideBar::DateTimeUtils</name>
    <message>
        <source>Just now</source>
        <translation type="vanished">刚刚</translation>
    </message>
    <message>
        <source>Within a week</source>
        <translation type="vanished">一周内</translation>
    </message>
    <message>
        <location filename="../notification/datetime/date-time-utils.cpp" line="108"/>
        <source>Now</source>
        <translation>现在</translation>
    </message>
    <message>
        <location filename="../notification/datetime/date-time-utils.cpp" line="119"/>
        <location filename="../notification/datetime/date-time-utils.cpp" line="121"/>
        <source>Yesterday </source>
        <translation>昨天 </translation>
    </message>
</context>
<context>
    <name>SideBarApplication</name>
    <message>
        <location filename="../main/side-bar-application.cpp" line="336"/>
        <source>Set up notification center</source>
        <translation>通知中心设置</translation>
    </message>
    <message>
        <location filename="../main/side-bar-application.cpp" line="363"/>
        <source>ukui-sidebar</source>
        <translation>侧边栏</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::PowerButton</name>
    <message>
        <location filename="../main/account-information.cpp" line="221"/>
        <source>power</source>
        <translation>电源</translation>
    </message>
</context>
</TS>
