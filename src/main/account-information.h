﻿/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef ACCOUNTINFORMATION_H
#define ACCOUNTINFORMATION_H

#include <QObject>
#include <QColor>
#include <QDBusInterface>

namespace UkuiShortcut {

typedef struct UserInfo
{
    bool current;
    bool isLogged;
    bool autologin;
    bool noPwdLogin;

    int accountType;
    int passwdType;
    qint64 uid;

    QString objPath;
    QString username;
    QString realName;
    QString iconFile;
    QString passwd;
} UserInformation;

class PowerButton : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString icon READ getIcon NOTIFY iconChanged)
    Q_PROPERTY(QString toolTip READ getToolTip NOTIFY iconChanged)
    Q_PROPERTY(QColor baseColor READ baseColor NOTIFY iconChanged)
    Q_PROPERTY(QColor highLightColor READ highLightColor NOTIFY iconChanged)
public:
    explicit PowerButton(QObject *parent = nullptr);
    ~PowerButton() override;

    QString getIcon();
    QString getToolTip();
    QColor baseColor();
    QColor highLightColor();

public Q_SLOTS:
    void clicked();

Q_SIGNALS:
    void iconChanged();
};

class AccountInformation : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString username READ getUsername NOTIFY userInfoChanged)
    Q_PROPERTY(QString realName READ getRealName NOTIFY userInfoChanged)
    Q_PROPERTY(QString iconFile READ getIconFile NOTIFY userInfoChanged)
    Q_PROPERTY(QString accountType READ getAccountType NOTIFY userInfoChanged)
public:
    explicit AccountInformation(QObject *parent = nullptr);

    ~AccountInformation() override;

    Q_INVOKABLE QString getUsername();
    Q_INVOKABLE QString getRealName();
    Q_INVOKABLE QString getIconFile();
    Q_INVOKABLE QString getAccountType();

private:
    void initMemberVariable();
    void registeredAccountsDbus();
    void initUserInfo();
    QStringList getUserObjectPath();
    UserInformation GetUserInformation(const QString &objectPath);

private Q_SLOTS:
    void currentAccountUpdateSlot(const QString &property, const QMap<QString, QVariant> &propertyMap,
                                  const QStringList &propertyList);

public Q_SLOTS:
    void openUserCenter();

Q_SIGNALS:
    void userInfoChanged();

private:
    QDBusInterface *m_systemUserIFace = nullptr;
    //用于监听当前登录用户的信息变化信号
    QDBusInterface *m_currentUserIFace = nullptr;

    UserInformation m_currentUserInfo;
    QString m_administrator;
    QString m_standardUser;
};

}

#endif // ACCOUNTINFORMATION_H
