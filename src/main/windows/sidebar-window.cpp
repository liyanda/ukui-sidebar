/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

//
// Created by hxf on 22-11-11.
//

#include "sidebar-window.h"
#include "screen-monitor.h"
#include "layout-config.h"

// kysdk
#include <kysdk/applications/waylandhelper.h>
#include <kysdk/applications/windowmanager/windowmanager.h>

// 窗管特效接口
#include <KWindowEffects>
#include <KWindowSystem>

// Qt
#include <QQuickItem>
#include <QX11Info>
#include <KWindowSystem>

#define HAND_GESTURE_WIDTH 2

namespace Sidebar {

static SidebarWindowHelper *globalWindowHelperInstance = nullptr;

void SidebarWindowDefineModule::defineModules(const char *uri, int versionMajor, int versionMinor)
{
    qRegisterMetaType<Sidebar::SidebarWindowType::Type>("SidebarWindowType::Type");
    qmlRegisterUncreatableType<Sidebar::SidebarWindowType>(uri, versionMajor, versionMinor, "SidebarWindowType", "Enum");

    qmlRegisterRevision<QWindow, 2>(uri, versionMajor, versionMinor);
    qmlRegisterRevision<QQuickWindow, 2>(uri, versionMajor, versionMinor);

    qmlRegisterType<Sidebar::SidebarWindow>(uri, versionMajor, versionMinor, "SidebarWindow");
    qmlRegisterType<Sidebar::StatusBarWindow>(uri, versionMajor, versionMinor, "StatusBarWindow");
    qmlRegisterType<Sidebar::RightHandGestureWindow>(uri, versionMajor, versionMinor, "RightHandGestureWindow");
    qmlRegisterType<Sidebar::NotificationCenterWindow>(uri, versionMajor, versionMinor, "NotificationCenterWindow");
}

SidebarWindowHelper *SidebarWindowHelper::instance(QObject *parent)
{
    if (!globalWindowHelperInstance) {
        globalWindowHelperInstance = new SidebarWindowHelper(parent);
    }

    return globalWindowHelperInstance;
}

SidebarWindowHelper::SidebarWindowHelper(QObject *parent) : QObject(parent)
{
    updateRects();

    ScreenMonitor *screenMonitor = ScreenMonitor::getInstance();
    connect(screenMonitor, &ScreenMonitor::geometryChanged, this, &SidebarWindowHelper::updateRects);
    connect(screenMonitor, &ScreenMonitor::primaryScreenChanged, this, &SidebarWindowHelper::updateRects);
}

void SidebarWindowHelper::setWindowGeometry(QWindow *window, const QRect& rect)
{
    if (!window) {
        qWarning() << "SidebarWindowHelper::updateWindowGeometry target window is null.";
        return;
    }

    if (QX11Info::isPlatformX11()) {
        window->setGeometry(rect);

    } else {
        kdk::WindowManager::setGeometry(window, rect);
    }
}

void SidebarWindowHelper::setWindowFlags(QWindow *window, SidebarWindowType::Type type)
{
    if (!window) {
        return;
    }

    switch (type) {
        default:
        case SidebarWindowType::Normal:
            break;
        case SidebarWindowType::Sidebar: {
            window->setFlags(Qt::FramelessWindowHint);
            break;
        }
        case SidebarWindowType::StatusBar:
        case SidebarWindowType::RightHandGesture:
        case SidebarWindowType::NotificationCenter: {
            window->setFlags(Qt::WindowStaysOnTopHint | Qt::X11BypassWindowManagerHint | Qt::FramelessWindowHint);
            break;
        }
    }
}

void SidebarWindowHelper::setWindowAttribute(QWindow *window, SidebarWindowType::Type type)
{
    // TODO 为每个窗口设置不同的属性（按需求）
    if (!window) {
        return;
    }

    switch (type) {
        default:
        case SidebarWindowType::Normal:
            return;
        case SidebarWindowType::Sidebar: {
            // 设置窗口层级
            KWindowSystem::setType(window->winId(), NET::Notification);
            break;
        }
        case SidebarWindowType::StatusBar:
        case SidebarWindowType::RightHandGesture: {
            KWindowSystem::setType(window->winId(), NET::OnScreenDisplay);
            break;
        }
        case SidebarWindowType::NotificationCenter: {
            KWindowSystem::setType(window->winId(), NET::Notification);
            break;
        }
    }

    // 窗口管理器属性
    KWindowSystem::setState(window->winId(), NET::SkipTaskbar | NET::SkipPager | NET::SkipSwitcher);
    if (!QX11Info::isPlatformX11()) {
        // wayland环境 设置跳过多任务视图，设置跳过任务栏
        kdk::WindowManager::setSkipTaskBar(window, true);
        kdk::WindowManager::setSkipSwitcher(window, true);
    }
}

QRect SidebarWindowHelper::getWindowGeometry(SidebarWindowType::Type type)
{
    switch (type) {
        case SidebarWindowType::Sidebar:
            return SidebarGeometryController::getInstance()->sidebarWindowGeometry();
        case SidebarWindowType::NotificationCenter:
            return m_notificationCenterRect;
        case SidebarWindowType::StatusBar:
            return m_statusBarRect;
        case SidebarWindowType::RightHandGesture:
            return m_rightGestureRect;
        default:
            return {};
    }
}

void SidebarWindowHelper::updateWindowGeometry(QWindow *window, SidebarWindowType::Type type)
{
    switch (type) {
        case SidebarWindowType::NotificationCenter:
            SidebarWindowHelper::setWindowGeometry(window, m_notificationCenterRect);
            break;
        case SidebarWindowType::StatusBar:
            SidebarWindowHelper::setWindowGeometry(window, m_statusBarRect);
            break;
        case SidebarWindowType::RightHandGesture:
            SidebarWindowHelper::setWindowGeometry(window, m_rightGestureRect);
            break;
        default:
            break;
    }
}

/**
 * 根据不同的窗口类型与主屏幕参数设置每一个窗口的geometry属性
 */
void SidebarWindowHelper::updateRects()
{
    QRect primaryScreenRect = ScreenMonitor::getInstance()->getGeometry();
    m_notificationCenterRect = primaryScreenRect;

    m_statusBarRect.setRect(primaryScreenRect.x(), primaryScreenRect.y(), primaryScreenRect.width(), HAND_GESTURE_WIDTH);

    m_rightGestureRect.setRect(primaryScreenRect.x() + primaryScreenRect.width() - HAND_GESTURE_WIDTH,
                               primaryScreenRect.y() + HAND_GESTURE_WIDTH,
                               HAND_GESTURE_WIDTH,
                               primaryScreenRect.height() - HAND_GESTURE_WIDTH);

    Q_EMIT geometryChanged();
}

/**
 * 所有窗口的基类，用于保证窗口的geometry始终与SidebarWindowHelper提供的参数一致
 * 屏蔽move事件，防止窗口被移动
 * @param parent
 */
SidebarWindowBase::SidebarWindowBase(QWindow *parent)
    : SidebarWindowBase(SidebarWindowType::Normal, parent)
{

}

/**
 * 每一个窗口都必须指定一个type所允许的类型
 * @param type 默认type为Normal，该类型不会触发任何动作规则
 * @param parent
 */
SidebarWindowBase::SidebarWindowBase(SidebarWindowType::Type type, QWindow *parent)
    : QQuickWindow(parent), m_windowType(type)
{
    initBase();
}

void SidebarWindowBase::initBase()
{
    if (m_windowType != SidebarWindowType::Normal) {
        SidebarWindowHelper::setWindowFlags(this, m_windowType);
        //SidebarWindowHelper::setWindowAttribute(this, m_windowType);

        if (m_windowType != SidebarWindowType::Sidebar) {
            updateGeometry();
            connect(SidebarWindowHelper::instance(), &SidebarWindowHelper::geometryChanged,
                    this, &SidebarWindowBase::updateGeometry);
        }
    }
}

bool SidebarWindowBase::event(QEvent *event)
{
    if (m_windowType != SidebarWindowType::Normal) {
        if (event->type() == QEvent::Move || event->type() == QEvent::Resize) {
            //防止窗口被挪动，可能是窗管或者wayland环境引起的pos改变
            updateGeometry();
            return true;
        }
    }

    return QQuickWindow::event(event);
}

SidebarWindowType::Type SidebarWindowBase::windowType()
{
    return m_windowType;
}

void SidebarWindowBase::updateGeometry()
{
    SidebarWindowHelper::setWindowGeometry(this, windowGeometry());
    QQuickItem *item = contentItem();
    if (item && item->size() != size()) {
        item->setSize(size());
    }
}

void SidebarWindowBase::exposeEvent(QExposeEvent *event)
{
    QQuickWindow::exposeEvent(event);
    updateGeometry();
}

QRect SidebarWindowBase::windowGeometry()
{
    return SidebarWindowHelper::instance()->getWindowGeometry(m_windowType);
}

StatusBarWindow::StatusBarWindow(QWindow *parent)
    : SidebarWindowBase(SidebarWindowType::StatusBar, parent)
{
    setColor("transparent");
    SidebarWindowHelper::setWindowAttribute(this, SidebarWindowType::StatusBar);
}

RightHandGestureWindow::RightHandGestureWindow(QWindow *parent)
    : SidebarWindowBase(SidebarWindowType::RightHandGesture, parent)
{
    setColor("transparent");
    SidebarWindowHelper::setWindowAttribute(this, SidebarWindowType::RightHandGesture);
}

NotificationCenterWindow::NotificationCenterWindow(QWindow *parent)
    : SidebarWindowBase(SidebarWindowType::NotificationCenter, parent)
{
    setColor("transparent");
    SidebarWindowHelper::setWindowAttribute(this, SidebarWindowType::NotificationCenter);
}

SidebarWindow::SidebarWindow(QWindow *parent)
    : SidebarWindowBase(SidebarWindowType::Sidebar, parent)
{
    initWindow();
}

bool SidebarWindow::contentVisible()
{
    return m_contentVisible;
}

void SidebarWindow::setContentVisible(bool visible)
{
    m_contentVisible = visible;

    if (m_contentVisible) {
        if (QX11Info::isPlatformX11()) {
            requestActivate();
        }
    }

    Q_EMIT contentVisibleChanged();
}

void SidebarWindow::initWindow()
{
    setTitle("ukui-sidebar");
    setColor("transparent");
    updateGeometry();
    SidebarWindowHelper::setWindowAttribute(this, SidebarWindowType::Sidebar);

    connect(SidebarGeometryController::getInstance(), &SidebarGeometryController::geometryChanged,
            this, &SidebarWindow::updateWindowGeometry);

    connect(LayoutConfig::getInstance(), &LayoutConfig::systemModeChanged,
            this, &SidebarWindow::isTabletModeChanged);
}

void SidebarWindow::updateWindowGeometry()
{
    updateGeometry();
    Q_EMIT sidebarContentGeometryChanged();
    Q_EMIT sidebarWindowGeometryChanged();
}

qint32 SidebarWindow::windowPadding()
{
    return SidebarGeometryController::getInstance()->sidebarWindowPadding();
}

qint32 SidebarWindow::minimumThreshold()
{
    return SidebarGeometryController::getInstance()->minimumThreshold();
}

qint32 SidebarWindow::primaryScreenRight()
{
    return SidebarGeometryController::getInstance()->primaryScreenRight();
}

QRect SidebarWindow::sidebarContentGeometry()
{
    return SidebarGeometryController::getInstance()->sidebarContentGeometry();
}

void SidebarWindow::focusInEvent(QFocusEvent *event)
{
    QQuickWindow::focusInEvent(event);
}

void SidebarWindow::focusOutEvent(QFocusEvent *event)
{
    // ukui环境快捷键会夺取焦点，如果按下的快捷键为win+A,那么侧边栏会先失去焦点隐藏，然后再被快捷键唤起，导致一进一出的问题
//    Q_EMIT requestHideContent();
    QQuickWindow::focusOutEvent(event);
}

bool SidebarWindow::isTabletMode()
{
    return LayoutConfig::getInstance()->isTabletMode();
}

QRect SidebarWindow::sidebarWindowGeometry()
{
    return SidebarGeometryController::getInstance()->sidebarWindowGeometry();
}


} // Sidebar
