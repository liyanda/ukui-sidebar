/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

//
// Created by hxf on 22-8-4.
//

#include "screen-monitor.h"
#include <mutex>

#include <QVariant>
#include <QApplication>
#include <QDebug>
using namespace Sidebar;
static std::once_flag flag;
static ScreenMonitor *global_intance = nullptr;
ScreenMonitor *ScreenMonitor::getInstance()
{
    std::call_once(flag, [ & ] {
       global_intance = new ScreenMonitor();
   });
    return global_intance;
}

int ScreenMonitor::getPanelPosition()
{
    return m_panelProperty.position;
}

int ScreenMonitor::getPanelSize()
{
    return m_panelProperty.size;
}

ScreenMonitor::ScreenMonitor(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<QScreen*>("QScreen*");
    m_primaryScreen = QApplication::primaryScreen();

    connect(qApp, &QApplication::screenAdded, this, &ScreenMonitor::screenAddedSlot);
    connect(qApp, &QApplication::screenRemoved, this, &ScreenMonitor::screenRemovedSlot);
    connect(qApp, &QApplication::primaryScreenChanged, this, &ScreenMonitor::primaryScreenChangedSlot);

    initPanelMonitor();

    primaryScreenChangedSlot(QApplication::primaryScreen());
}

void ScreenMonitor::initPanelMonitor()
{
    const QByteArray id(UKUI_PANEL_SETTING);
    if (QGSettings::isSchemaInstalled(id)) {
        m_panelSetting = new QGSettings(id, QByteArray(), this);

        QStringList keys = m_panelSetting->keys();
        if (keys.contains(UKUI_PANEL_POSITION_KEY)) {
            m_panelProperty.position = m_panelSetting->get(UKUI_PANEL_POSITION_KEY).toInt();
        }
        if (keys.contains(UKUI_PANEL_SIZE_KEY)) {
            m_panelProperty.size = m_panelSetting->get(UKUI_PANEL_SIZE_KEY).toInt();
        }

        connect(m_panelSetting, &QGSettings::changed, this, &ScreenMonitor::panelPropertyChangedSlot);
    }
}

void ScreenMonitor::primaryScreenChangedSlot(QScreen *screen)
{
    if (!screen) {
        return;
    }

    if (m_primaryScreen) {
        disconnect(m_primaryScreen);
    }

    m_primaryScreen = screen;
    connect(m_primaryScreen, &QScreen::geometryChanged, this, &ScreenMonitor::geometryChanged);

    Q_EMIT primaryScreenChanged();
    Q_EMIT geometryChanged(m_primaryScreen->geometry());
}

void ScreenMonitor::screenRemovedSlot(QScreen *screen)
{
    Q_UNUSED(screen)
    qDebug() << "ScreenMonitor::onScreenRemoved";
//    if (screen == m_primaryScreen) {
//        m_primaryScreen = QApplication::primaryScreen();
//    }
}

void ScreenMonitor::screenAddedSlot(QScreen *screen)
{
    Q_UNUSED(screen)
    qDebug() << "ScreenMonitor::onScreenAdded";
}

void ScreenMonitor::panelPropertyChangedSlot(const QString& key)
{
    if (key == UKUI_PANEL_POSITION_KEY || key == UKUI_PANEL_SIZE_KEY) {
        if (key == UKUI_PANEL_POSITION_KEY) {
            m_panelProperty.position = m_panelSetting->get(UKUI_PANEL_POSITION_KEY).toInt();
        } else {
            m_panelProperty.size = m_panelSetting->get(UKUI_PANEL_SIZE_KEY).toInt();
        }

        Q_EMIT panelPropertyChanged();
    }
}

QScreen *ScreenMonitor::getPrimaryScreen()
{
    return m_primaryScreen;
}

QRect ScreenMonitor::getGeometry()
{
    if (m_primaryScreen) {
        return m_primaryScreen->geometry();
    }
    return {};
}
