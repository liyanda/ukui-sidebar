/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

//
// Created by hxf on 22-8-2.
//

#ifndef UKUI_SIDEBAR_SIDE_BAR_APPLICATION_H
#define UKUI_SIDEBAR_SIDE_BAR_APPLICATION_H

//注册托盘
#define  TRAY_ICON           ":/icon/kylin-tool-box.svg"
#define  TRAY_NULL_ICON      ":/icon/kylin-tool-box-null.svg"
#define  SETTING_ICON        ":/icon/application-menu.svg"

//注册dbus接口
#define SIDEBAR_CORE_DBUS_SERVICE          "org.ukui.Sidebar"
#define SIDEBAR_CORE_DBUS_PATH             "/org/ukui/Sidebar"
//#define SIDEBAR_CORE_DBUS_INTERFACE        "org.ukui.Sidebar"

#include <QObject>
#include <QUrl>
#include <QQmlApplicationEngine>
#include <QSystemTrayIcon>
#include <QAction>
#include <QMenu>
#include <QProcess>
#include <QTimer>
#include <QDBusInterface>
#include <QObject>
#include <QDBusConnection>
#include <QTranslator>

#include "qtsingleapplication.h"
#include "color-helper.h"
#include "layout-config.h"
#include "plugin-common-data.h"


class SidebarDbusService;
class SidebarEventBridge;
class NotificationEventBridge;

class SideBarApplication : public QtSingleApplication
{
    friend class SidebarDbusService; //声明d-bus模块为友元类
    friend class SidebarEventBridge;
    friend class NotificationEventBridge;
    Q_OBJECT
public:
    explicit SideBarApplication(const QString &appId, int &argc, char **argv);
    ~SideBarApplication();

private Q_SLOTS:
    void checkObject(QObject *object, const QUrl &url);
    void startSidebar();
    void execShortcutAction(UkuiShortcut::PluginMetaType::PredefinedAction action);

private:
    void parseCommand(const QString &msg, bool isPrimary);
    static void registerItems();
    static bool checkOpenGLVersion();

    inline void initPublicObjects();
    void loadQML();
    void waitTime();
    //系统托盘
    void createActionForTrayIcon();                                                        // 连接信号和槽函数，设置其动作;
    void createSystray();                                                       // 设置menu界面、添加动作 和 创建sysytray实例
    void OpenSidebarSlots();
    void OpenControlCenterSettings();
    void changeIcon();
    void changeIconBack();

private:
    QQmlApplicationEngine *m_shortcutEngine = nullptr;
    QQmlApplicationEngine *m_notificationEngine = nullptr;
    QQmlApplicationEngine *m_statusBarEngine = nullptr;
    QQmlApplicationEngine *m_right2LeftSwipeEngine = nullptr;

    const QUrl m_shortcutQmlFile = {QStringLiteral("qrc:/qml/main.qml")};
    const QUrl m_notificationQmlFile = {QStringLiteral("qrc:/qml/NotificationCenter.qml")};
    const QUrl m_statusBarQmlFile = {QStringLiteral("qrc:/qml/StatusBar.qml")};
    const QUrl m_right2LeftSwipeQmlFile = {QStringLiteral("qrc:/qml/Right2LeftSwipe.qml")};

    QTranslator *m_translator = nullptr;
    QTimer*                     m_timer = nullptr;
    //系统托盘
    QSystemTrayIcon*            m_trayIcon = nullptr;
    QMenu*                      m_trayIconMenu = nullptr;
    SidebarDbusService*         m_sidebarDbusService = nullptr;
    SidebarEventBridge*         m_sidebarEventBridge = nullptr;
    NotificationEventBridge*    m_notificationEventBridge = nullptr;
};

class SidebarDbusService : public QObject
{
    Q_OBJECT
    //申明该类有D-BUS服务接口
    Q_CLASSINFO("D-Bus Interface", "org.ukui.Sidebar")
public:
    explicit SidebarDbusService(QObject *parent = nullptr);                    //传入父指针，调用

public Q_SLOTS:
    void sidebarActive();
    void showSidebar(QString arg);
    void hideSidebar(QString arg);

private:
    SideBarApplication* m_sidebarApplication = nullptr;
};

/**
 * 用于转发sidebar发出的事件，或者从window中暴露接口供调用
 */
class SidebarEventBridge : public QObject
{
    Q_OBJECT
public:
    explicit SidebarEventBridge(QObject *parent = nullptr);
    bool getSidebarState();

private:
    bool m_sidebarIsShowed = false;

public Q_SLOTS:
    void activateSidebar();
    void sidebarOnlyShow();
    void sidebarOnlyHide();
    void hideSidebarQuickly();
    void clearSidebarRedPoint(bool isShowed);

Q_SIGNALS:
    void requestActivateSidebar();
    void requestShowSidebar();
    void requestHideSidebar();
    void requestHideQuickly();
    void sidebarRequestClearRedPoint();
};

class NotificationEventBridge : public QObject
{
    Q_OBJECT
public:
    explicit NotificationEventBridge(QObject *parent = nullptr);
    bool getNotificationCenterState();

private:
    bool m_notificationCenterIsShowed = false;

public Q_SLOTS:
    void notificationShow();
    void notificationHide();
    void clearNotificationCenterRedPoint(bool isShowed);

Q_SIGNALS:
    void requestShowNotification();
    void requestHideNotification();
    void notificationCenterRequestClearRedPoint();
};
#endif //UKUI_SIDEBAR_SIDE_BAR_APPLICATION_H
