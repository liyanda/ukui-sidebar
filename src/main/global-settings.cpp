/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

//
// Created by hxf on 22-8-16.
//

#include "global-settings.h"

#include <QDebug>
#include <QDBusReply>

using namespace Sidebar;

static GlobalSettings *g_globalSettings = nullptr;

GlobalSettings *GlobalSettings::globalInstance(QObject *parent)
{
    if (!g_globalSettings) {
        g_globalSettings = new GlobalSettings(parent);
    }

    return g_globalSettings;
}

GlobalSettings::GlobalSettings(QObject *parent) : QObject(parent)
{
    // 1.加载主题 gsetting
    initStyleSettings();
    initControlCenterSettings();
    // 2.加载平板模式设置
    initStatusManagerDbus();
}

GlobalSettings::~GlobalSettings()
{

}

const QVariant GlobalSettings::getValue(const QString &key)
{
    if (m_cache.contains(key)) {
        return m_cache.value(key);
    }

    return {};
}

void GlobalSettings::initStyleSettings()
{
    insertValue(UKUI_STYLE_NAME_KEY, DEFAULT_STYLE);

    const QByteArray id(UKUI_STYLE_SETTING);
    if (QGSettings::isSchemaInstalled(id)) {
        auto *settings = new QGSettings(id, QByteArray(), this);
        connect(settings, &QGSettings::changed, this, [=](const QString &key) {
            if (key == UKUI_STYLE_NAME_KEY || key == UKUI_STYLE_THEME_COLOR_KEY) {
                insertValue(key, settings->get(key));
                Q_EMIT valueChanged(key);
            }
        });

        QStringList keys = settings->keys();
        if (keys.contains(UKUI_STYLE_NAME_KEY)) {
            insertValue(UKUI_STYLE_NAME_KEY, settings->get(UKUI_STYLE_NAME_KEY));
        }
    }
}

void GlobalSettings::insertValue(const QString &key, const QVariant &value)
{
    m_cache.insert(key,value);
}

void GlobalSettings::initStatusManagerDbus()
{
    m_cache.insert(TABLET_MODE, false);
    //dbus
    m_statusManagerDBus = new QDBusInterface(DBUS_STATUS_MANAGER_IF, "/" , DBUS_STATUS_MANAGER_IF, QDBusConnection::sessionBus(), this);
    if (m_statusManagerDBus && m_statusManagerDBus->isValid()) {
        //平板模式切换
        connect(m_statusManagerDBus, SIGNAL(mode_change_signal(bool)), this, SLOT(updateTabletStatus(bool)));

        QDBusReply<bool> message = m_statusManagerDBus->call("get_current_tabletmode");
        if (message.isValid()) {
            m_cache.insert(TABLET_MODE, message.value());
        }
    }
}

void GlobalSettings::updateTabletStatus(bool isTabletMode)
{
    m_cache.insert(TABLET_MODE, isTabletMode);
    Q_EMIT valueChanged(TABLET_MODE);
}

void GlobalSettings::initControlCenterSettings()
{
    insertValue(CONTROL_CENTER_EFFECT, false);
    insertValue(CONTROL_CENTER_TRANSPARENCY_KEY, 1.0);
    const QByteArray id(CONTROL_CENTER_SETTING);
    if (QGSettings::isSchemaInstalled(id)) {
        auto *settings = new QGSettings(id, QByteArray(), this);
        connect(settings, &QGSettings::changed, this, [=](const QString &key) {
            if (key == CONTROL_CENTER_TRANSPARENCY_KEY || key == CONTROL_CENTER_EFFECT) {
                insertValue(key, settings->get(key));
                Q_EMIT valueChanged(key);
            }
        });

        QStringList keys = settings->keys();
        if (keys.contains(CONTROL_CENTER_TRANSPARENCY_KEY)) {
            insertValue(CONTROL_CENTER_TRANSPARENCY_KEY, settings->get(CONTROL_CENTER_TRANSPARENCY_KEY));
        }
        if (keys.contains(CONTROL_CENTER_EFFECT)) {
            insertValue(CONTROL_CENTER_EFFECT, settings->get(CONTROL_CENTER_EFFECT));
        }
    }
}

SettingMonitor::SettingMonitor(QObject *parent) : QObject(parent)
{
    connect(GlobalSettings::globalInstance(), &GlobalSettings::valueChanged,
            this, &SettingMonitor::valueChangedSlot);
}

QStringList SettingMonitor::keys()
{
    return m_keys;
}

void SettingMonitor::setKeys(QStringList &keys)
{
    if (keys == m_keys) {
        return;
    }

    m_keys.clear();
    m_keys.append(keys);

    Q_EMIT keysChanged();
}

void SettingMonitor::valueChangedSlot(const QString &key)
{
    if (m_keys.contains(key)) {
        Q_EMIT valueChanged(key);
    }
}

QVariant SettingMonitor::getValue(const QString &key)
{
    return GlobalSettings::globalInstance()->getValue(key);
}
