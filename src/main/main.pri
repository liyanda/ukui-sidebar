INCLUDEPATH += $$PWD

include($$PWD/windows/windows.pri)

HEADERS += \
    $$PWD/side-bar-application.h \
    $$PWD/screen-monitor.h \
    $$PWD/color-helper.h \
    $$PWD/account-information.h \
    $$PWD/global-settings.h \
    $$PWD/hand-gesture-helper.h \
    $$PWD/window-blur-helper.h \
    $$PWD/app-manager.h

SOURCES += \
    $$PWD/side-bar-application.cpp \
    $$PWD/screen-monitor.cpp \
    $$PWD/color-helper.cpp \
    $$PWD/account-information.cpp \
    $$PWD/global-settings.cpp \
    $$PWD/hand-gesture-helper.cpp \
    $$PWD/window-blur-helper.cpp \
    $$PWD/app-manager.cpp
