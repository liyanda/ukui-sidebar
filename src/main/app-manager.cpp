/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

//
// Created by hxf on 22-10-11.
//

#include "app-manager.h"

#include <QDebug>
#include <QProcess>
#include <QDBusReply>
#include <QDBusInterface>

#define KYLIN_APP_MANAGER_NAME         "com.kylin.AppManager"
#define KYLIN_APP_MANAGER_PATH         "/com/kylin/AppManager"
#define KYLIN_APP_MANAGER_INTERFACE    "com.kylin.AppManager"

namespace Sidebar {

static AppManager *globalInstance = nullptr;

AppManager *AppManager::getInstance(QObject *parent)
{
    if (!globalInstance) {
        globalInstance = new AppManager(parent);
    }

    return globalInstance;
}

AppManager::AppManager(QObject *parent) : QObject(parent)
{
    m_appManagerDbusInterface = new QDBusInterface(KYLIN_APP_MANAGER_NAME,
                                                   KYLIN_APP_MANAGER_PATH,
                                                   KYLIN_APP_MANAGER_INTERFACE,
                                                   QDBusConnection::sessionBus());

    if (!m_appManagerDbusInterface) {
        qWarning() << "account info: appmanager dbus does not exists.";
    }
}

bool AppManager::LaunchApp(const QString& desktopFile)
{
    if (m_appManagerDbusInterface != nullptr) {
        QDBusReply<bool> status = m_appManagerDbusInterface->call("LaunchApp", desktopFile);
        return status;
    } else {
        qWarning()<<"LaunchApp is failed,return false";
        return false;
    }
}

bool AppManager::LaunchAppWithArguments(const QString &desktopFile, const QStringList &args)
{
    if (m_appManagerDbusInterface != nullptr) {
        QDBusReply<bool> status = m_appManagerDbusInterface->call("LaunchAppWithArguments",desktopFile,args);
        return status;
    } else {
        qWarning()<<"LaunchAppWithArguments is failed,return false";
        return false;
    }
}

AppManager::~AppManager()
= default;

} // Sidebar
