/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

//
// Created by hxf on 22-7-29.
//

#ifndef UKUI_SIDEBAR_SHORTCUT_MODEL_MANAGER_H
#define UKUI_SIDEBAR_SHORTCUT_MODEL_MANAGER_H

#include "short-cut-manager.h"

namespace UkuiShortcut {

class ShortcutModel;

class ShortcutModelManager : public QObject
{
    Q_OBJECT
public:
    explicit ShortcutModelManager(QObject *parent = nullptr);
    Q_INVOKABLE UkuiShortcut::ShortcutModel *getModel(PluginMetaType::PluginType);

Q_SIGNALS:
    void requestExecAction(UkuiShortcut::PluginMetaType::PredefinedAction action);

private:
    ShortcutManager *m_shortcutManager = nullptr;
    QMap<PluginMetaType::PluginType, ShortcutModel*> m_models;
    PluginMetaType::SystemMode m_currentMode = PluginMetaType::PC;

private Q_SLOTS:
    void updateShortcutEnable(UkuiShortcut::UkuiShortcutPlugin* plugin, bool isEnable);
    void updateCurrentMode(const QString &key);

private:
    void updateShortCutModel(PluginMetaType::SystemMode oldMode);
};

} // UkuiShortcut

#endif //UKUI_SIDEBAR_SHORTCUT_MODEL_MANAGER_H
