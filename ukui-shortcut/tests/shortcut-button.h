#ifndef SHORTCUTBUTTON_H
#define SHORTCUTBUTTON_H

#include <QObject>
#include <QPushButton>
#include <QColor>

class ShortcutButton : public QPushButton
{
    Q_OBJECT
public:
    ShortcutButton(QWidget *parent = nullptr);
    void setIconName(const QString &icon);
    void setColor(const QColor &color);

};

#endif // SHORTCUTBUTTON_H
