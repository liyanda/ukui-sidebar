#include "main-window.h"
#include <QDebug>

#include "status-info.h"

#include "progress-bar.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    m_centralWidget = new QWidget(this);
    this->setCentralWidget(m_centralWidget);
    m_layout = new QVBoxLayout(m_centralWidget);

    m_shortcutManager = UkuiShortcut::ShortcutManager::getInstance();
    m_shortcutPlugins = m_shortcutManager->getShortcuts();
    for(auto plugin : m_shortcutPlugins) {
        connect(plugin, &UkuiShortcut::UkuiShortcutPlugin::enableStatusChanged, [ = ](bool isEnable){
            qDebug() << plugin << "isEnable" << isEnable;
        });
//        if(!plugin->isEnable()) {
//            continue;
//        }
        UkuiShortcut::StatusInfo si = plugin->currentStatus();
        qDebug() << "find shortcut :" << plugin->pluginId();

        if (plugin->pluginType() == UkuiShortcut::UkuiShortcutPlugin::PluginType::Icon) {
            Shortcut *shortcut = new Shortcut(si.getName(), si.getIcon(), si.getColor(), this);
            connect(plugin, &UkuiShortcut::UkuiShortcutPlugin::statusChanged, shortcut, &Shortcut::statusChanged);
            connect(shortcut, &Shortcut::clicked, plugin, [=] {
                plugin->active(UkuiShortcut::UkuiShortcutPlugin::Click);
            });
            m_layout->addWidget(shortcut, 0, Qt::AlignHCenter);

        } else if(plugin->pluginType() == UkuiShortcut::UkuiShortcutPlugin::PluginType::ProgressBar){
            ProgressBar *progressBar = new ProgressBar(this);
            progressBar->setValue(si.getValue());
            progressBar->setIcon(si.getIcon());

            m_layout->addWidget(progressBar, 0, Qt::AlignHCenter);

            connect(plugin, &UkuiShortcut::UkuiShortcutPlugin::statusChanged, progressBar, [=] (const UkuiShortcut::StatusInfo &statusInfo) {
                progressBar->setValue(statusInfo.getValue());
                progressBar->setIcon(statusInfo.getIcon());
            });

            connect(progressBar, &ProgressBar::add, plugin,[=](int size) {
                plugin->setValue(size);
            });
        } else if(plugin->pluginType() == UkuiShortcut::UkuiShortcutPlugin::PluginType::MenuButton) {
            qDebug() << plugin->pluginId() <<plugin->isEnable();
            if(plugin->isEnable()) {
                qDebug() << plugin->pluginId() << "-------" << plugin->currentStatus().getName();
                qDebug() << plugin->pluginId() << "-------" << plugin->currentStatus().getIcon();
            }
        }
    }


}

MainWindow::~MainWindow()
{
}
