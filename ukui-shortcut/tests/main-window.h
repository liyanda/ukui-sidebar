#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QPushButton>
#include <QVBoxLayout>
#include "short-cut-manager.h"
#include "shortcut.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    UkuiShortcut::ShortcutManager *m_shortcutManager = nullptr;
    QList<UkuiShortcut::UkuiShortcutPlugin*> m_shortcutPlugins;
    QWidget *m_centralWidget = nullptr;
    QVBoxLayout *m_layout = nullptr;
    QList<Shortcut *> m_shortcuts;

};
#endif // MAINWINDOW_H
