#ifndef SHORTCUT_H
#define SHORTCUT_H

#include <QObject>
#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QString>
#include <QColor>
#include "shortcut-button.h"
#include "status-info.h"

class Shortcut : public QWidget
{
    Q_OBJECT
public:
    explicit Shortcut(QString displayName, QString iconName, QColor color, QWidget *parent = nullptr);
public Q_SLOTS:
    void statusChanged(UkuiShortcut::StatusInfo info);
Q_SIGNALS:
    void clicked();

private:
    QVBoxLayout *m_layout = nullptr;
    QLabel *m_nameLabel = nullptr;
    ShortcutButton * m_button = nullptr;

};

#endif // SHORTCUT_H
