#include "shortcut.h"
#include <QWidget>

Shortcut::Shortcut(QString displayName, QString iconName, QColor color, QWidget *parent) : QWidget(parent)
{
    this->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    m_layout = new QVBoxLayout(this);

    m_button = new ShortcutButton(this);
    m_button->setIconName(iconName);
    m_button->setColor(color);
    connect(m_button, &ShortcutButton::clicked, this, &Shortcut::clicked);
    m_nameLabel = new QLabel(displayName, this);

    m_layout->addWidget(m_button, Qt::AlignHCenter);
    m_layout->addWidget(m_nameLabel, Qt::AlignHCenter);
}

void Shortcut::statusChanged(UkuiShortcut::StatusInfo info)
{
    m_button->setIconName(info.getIcon());
    m_button->setColor(info.getColor());
    m_nameLabel->setText(info.getName());
}
