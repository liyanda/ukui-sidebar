//
// Created by hxf on 22-7-21.
//

#ifndef UKUI_SIDEBAR_PROGRESS_BAR_H
#define UKUI_SIDEBAR_PROGRESS_BAR_H

#include <QWidget>
#include <QProgressBar>
#include <QPushButton>
#include <QLabel>

class ProgressBar : public QWidget
{
    Q_OBJECT
public:
    explicit ProgressBar(QWidget *parent = nullptr);

    void setIcon(const QString &icon);

public Q_SLOTS:
    void setValue(int size);

Q_SIGNALS:
    void add(int);

private:
    QProgressBar *m_progressBar = nullptr;
    QPushButton *m_button = nullptr;
    QLabel *m_label = nullptr;
};


#endif //UKUI_SIDEBAR_PROGRESS_BAR_H
