#include "shortcut-button.h"
#include <QPalette>
#include <QVariant>
#include <QDebug>

ShortcutButton::ShortcutButton(QWidget *parent) : QPushButton(parent)
{
    setFixedSize(56, 56);
    setIconSize(QSize(24, 24));
    setProperty("isRoundButton",true);
}

void ShortcutButton::setIconName(const QString &icon)
{
    qDebug() << icon;
    this->setIcon(QIcon::fromTheme(icon));
}

void ShortcutButton::setColor(const QColor &color)
{
    QPalette pa = this->palette();
    pa.setColor(QPalette::Normal, QPalette::Button, color);
    this->setPalette(pa);
}
