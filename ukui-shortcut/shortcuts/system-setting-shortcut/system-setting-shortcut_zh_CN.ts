<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>UkuiShortcut::SystemSetting</name>
    <message>
        <source>Settings</source>
        <oldsource>system setting</oldsource>
        <translation type="vanished">系统设置</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::SystemSettingShortcut</name>
    <message>
        <location filename="system-setting-shortcut.cpp" line="24"/>
        <location filename="system-setting-shortcut.cpp" line="26"/>
        <source>Settings</source>
        <translation>系统设置</translation>
    </message>
</context>
</TS>
