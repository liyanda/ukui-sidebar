/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef WIFISHORTCUT_H
#define WIFISHORTCUT_H

#define CONTROL_CENTER_WIFI "org.ukui.control-center.wifi.switch"
#define KYLIN_WIFI_GSETTING_VALUE "org.kylinnm.settings"

#define KYLIN_WIFI_PATH         "network-wireless-signal-excellent-symbolic"

#define KYLIN_APP_MANAGER_NAME         "com.kylin.AppManager"
#define KYLIN_APP_MANAGER_PATH         "/com/kylin/AppManager"
#define KYLIN_APP_MANAGER_INTERFACE    "com.kylin.AppManager"

#define KYLIN_NETWORK_NAME             "com.kylin.network"
#define KYLIN_NETWORK_PATH             "/com/kylin/network"
#define KYLIN_NETWORK_INTERFACE        "com.kylin.network"

#define NETWORK_MANAGER_NAME                "org.freedesktop.NetworkManager"
#define NETWORK_MANAGER_PATH                "/org/freedesktop/NetworkManager"
#define NETWORK_MANAGER_INTERFACE           "org.freedesktop.NetworkManager"
#define NETWORK_DBUS_PROPERTIES_INTERFACE   "org.freedesktop.DBus.Properties"

#include "ukui-shortcut-plugin.h"
#include "wifi-dbus-query.h"

#include <QtDBus/QDBusInterface>
#include <QObject>
#include <QProcess>
#include <QDBusInterface>
#include <QDBusReply>
#include <QGSettings>
#include <QThread>

namespace UkuiShortcut {

class WiFiShortcut : public UkuiShortcutPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID UkuiShortcutPluginIface_iid FILE "wifi-shortcut.json")
    Q_INTERFACES(UkuiShortcut::UkuiShortcutPlugin)

public:
    explicit WiFiShortcut(QObject *parent = nullptr);
    ~WiFiShortcut() override;
    QString pluginId() override;
    QMap<PluginMetaType::SystemMode, PluginMetaData> pluginMetaData() override;
    void active(PluginMetaType::Action action) override;
    const StatusInfo currentStatus() override;
    bool isEnable() override;

private:
    inline void initMetaData();
    void initMemberVariables();
    void initNetworkManagerConnect();

    void networkDbus();
    void getUsedWIFIConnections();
    void updateIcon(QString wifiName);

    void kywifibegin();
    void appManagerDbus();
    bool launchAppWithArguments(QString desktopFile,QStringList args);

    void wifiButtonDbus();
    void kyWifiButtonActive();

private Q_SLOTS:
    void onPropertiesChanged(const QString &propertyType, const QVariantMap &propertyContent);
    void onDevicestatusChanged();

    void handleDeviceQuery(bool isUsed);
    void handleUsedWifiConnect(QString wifiName);

    void getButtonState();

private:
    QMap<PluginMetaType::SystemMode, PluginMetaData> m_metaData;
    StatusInfo m_currentStatus;
    QDBusInterface *m_appManagerDbusInterface = nullptr;
    QDBusInterface *m_networkDbusInterface = nullptr;
    //wifi显示字段
    QString      m_wired_connection = QObject::tr("Wired connection");
    QString      m_notConnected = QObject::tr("Not connected");
    QString      m_closed = QObject::tr("Closed");
    bool         m_wirelessDeviceIsEnable = false;

    QThread       *m_wifiDbusThread = nullptr;
    WifiDbusQuery *m_wifiDbusQuery = nullptr;

    bool      m_wifiButtonIsOpen = false;
    QDBusInterface *m_wifiButtonDbusInterface = nullptr;

Q_SIGNALS:
    void toQueryDevice();
    void toQueryUsedWifiConnect(QDBusInterface *networkDbusInterface);
};


}
#endif // NIGHTMODESHORTCUT_H
