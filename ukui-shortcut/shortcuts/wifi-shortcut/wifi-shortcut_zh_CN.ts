<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <source>Wired connection</source>
        <translatorcomment>无线网络已连接</translatorcomment>
        <translation>无线网络已连接</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translatorcomment>无线网络未连接</translatorcomment>
        <translation>无线网络未连接</translation>
    </message>
    <message>
        <source>Closed</source>
        <translatorcomment>无线网络不可用</translatorcomment>
        <translation>无线网络不可用</translation>
    </message>
</context>
<context>
    <name>UkuiShortcut::WiFiShortcut</name>
    <message>
        <source>WiFi</source>
        <translatorcomment>无线局域网</translatorcomment>
        <translation>无线局域网</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="unfinished">无线网络未连接</translation>
    </message>
</context>
</TS>
