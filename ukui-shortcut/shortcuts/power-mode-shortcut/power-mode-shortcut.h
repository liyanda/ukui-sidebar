/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef POWERMODESHORTCUT_H
#define POWERMODESHORTCUT_H
#include <QObject>
#include <QDBusInterface>
#include "ukui-shortcut-plugin.h"

namespace UkuiShortcut {
class PowerModeShortcut : public UkuiShortcutPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID UkuiShortcutPluginIface_iid FILE "power-mode-shortcut.json")
    Q_INTERFACES(UkuiShortcut::UkuiShortcutPlugin)

public:
    PowerModeShortcut();
    ~PowerModeShortcut() override;

    QString pluginId() override {return QStringLiteral("PowerModeShortcut");}
    QMap<PluginMetaType::SystemMode, PluginMetaData> pluginMetaData() override;
    void active(PluginMetaType::Action action) override;
    const StatusInfo currentStatus() override;
    bool isEnable() override {return m_isEnable;}

private Q_SLOTS:
    void stateChangedSlot(QStringList keys);

private:
    inline void initMetaData();

private:
    void setState(const int state);
    StatusInfo m_currentStatus;
    QDBusInterface *m_powerManagerIface = nullptr;
    bool m_isEnable = false;
    int m_powerState = 0;
    QMap<PluginMetaType::SystemMode, PluginMetaData> m_metaData;
};
}
#endif // POWERMODESHORTCUT_H
