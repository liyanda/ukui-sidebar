QT += core gui dbus
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = lib
CONFIG += plugin
TARGET = screenshot-shortcut 
DEFINES += SCREENSHOTSHORTCUT_LIBRARY

CONFIG += c++11 lrelease
include($$PWD/../../libukui-shortcut-common.pri)
# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    screenshot-shortcut.cpp

HEADERS += \
    screenshot-shortcut.h

TRANSLATIONS += \
    screenshot-shortcut_zh_CN.ts

TRANSLATION_FILE_DIR = $${SHORTCUT_DATA_INSTALL_DIR}/$${TARGET}/translations
DEFINES += TRANSLATION_FILE_DIR='\\"$${TRANSLATION_FILE_DIR}\\"'

# 安装在编译过程中才会生成的文件需要指定 'no_check_exist'，否则不会在Makefile中生成对应的install规则
qm_files.CONFIG += no_check_exist
qm_files.path = $${TRANSLATION_FILE_DIR}

# 对每一个翻译文件生成对应的 make install 规则
for(file, TRANSLATIONS) {
    # 提取每一个.ts文件的文件名
    TS_FILE = $$basename(file)
    # 转换对应的.qm文件
    QM_FILE = $$replace(TS_FILE, ".ts", ".qm")

    qm_files.files += $${OUT_PWD}/.qm/$${QM_FILE}
    # message(QM_FILE = $${QM_FILE})
}

INSTALLS += qm_files

DISTFILES += screenshot-shortcut.json

#rules for deployment.
target.path = $$[QT_INSTALL_LIBS]/ukui-shortcut-plugins

INSTALLS += target
LIBS += -L$$OUT_PWD/../../ -lukui-shortcut

INCLUDEPATH += $$PWD/../../
DEPENDPATH += $$PWD/../../
