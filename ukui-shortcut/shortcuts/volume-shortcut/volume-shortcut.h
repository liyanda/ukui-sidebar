/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */
#ifndef VOLUMESHORTCUT_H
#define VOLUMESHORTCUT_H
#include <QGSettings>
#include "ukui-shortcut-plugin.h"

namespace UkuiShortcut {
class  VolumeShortcut : public UkuiShortcutPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID UkuiShortcutPluginIface_iid FILE "volume-shortcut.json")
    Q_INTERFACES(UkuiShortcut::UkuiShortcutPlugin)
public:
    explicit VolumeShortcut(QObject *parent = nullptr);
    ~VolumeShortcut() override;

    QString pluginId() override {return QStringLiteral("VolumeShortcut");}
    QMap<PluginMetaType::SystemMode, PluginMetaData> pluginMetaData() override;
    void active(PluginMetaType::Action action) override;
    void setValue(int value) override;
    const StatusInfo currentStatus() override;
    bool isEnable() override;

private:
    void changeValue(int volume);
    void updateStatus(int volume);
    void mute(bool mute);
    inline void initMetaData();

    QGSettings *m_gsettings = nullptr;
    bool m_isEnable = false;
    bool m_mute = false;
    StatusInfo m_currentStatusInfo;
    QMap<PluginMetaType::SystemMode, PluginMetaData> m_metaData;
};
}
#endif // VOLUMESHORTCUT_H
