/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */
#include "volume-shortcut.h"
#include <QDebug>
#include <QApplication>
#include <QTranslator>
using namespace UkuiShortcut;

#define USD_PLUGIN_MEDIA_KEY_STATE                "org.ukui.SettingsDaemon.plugins.media-keys-state"
#define VOLUME_KEY                                "sinkVolume"
#define MUTE_KEY                                  "sinkMute"
#define AUDIO_VOLUME_HIGH_SYMBOLIC                "audio-volume-high-symbolic"
#define AUDIO_VOLUME_LOW_SYMBOLIC                 "audio-volume-low-symbolic"
#define AUDIO_VOLUME_MEDIUM_SYMBOLIC              "audio-volume-medium-symbolic"
#define AUDIO_VOLUME_MUTED_SYMBOLIC               "audio-volume-muted-symbolic"

VolumeShortcut::VolumeShortcut(QObject *parent) : UkuiShortcutPlugin(parent)
{
    QTranslator *translator = new QTranslator(this);
    try {
        if(!translator->load(QString(TRANSLATION_FILE_DIR) + "/volume-shortcut_" + QLocale::system().name() + ".qm")) throw -1;
        QApplication::installTranslator(translator);
    } catch(...) {
        qWarning() << "VolumeShortcut load translations file" << QLocale::system().name() << "failed!";
    }

    initMetaData();

    m_currentStatusInfo.setValue(0);
    m_currentStatusInfo.setIcon(AUDIO_VOLUME_MUTED_SYMBOLIC);
    m_currentStatusInfo.setToolTip(tr("Volume"));

    const QByteArray id(USD_PLUGIN_MEDIA_KEY_STATE);
    if(QGSettings::isSchemaInstalled(id)) {
        m_gsettings = new QGSettings(id);

        if(m_gsettings->keys().contains(VOLUME_KEY, Qt::CaseInsensitive)) {
            changeValue(static_cast<int>(m_gsettings->get(VOLUME_KEY).toDouble()));
            m_isEnable = true;
        } else {
            qWarning() << "VolumeShortcut:" << "can't find key :" << VOLUME_KEY;
            m_isEnable = false;
        }

        if(m_gsettings->keys().contains(MUTE_KEY, Qt::CaseInsensitive)) {
            mute(m_gsettings->get(MUTE_KEY).toBool());
        } else {
            qWarning() << "VolumeShortcut:" << "can't find key :" << MUTE_KEY;
        }

        connect(m_gsettings, &QGSettings::changed, this, [ & ](const QString & key) {
            if(key == VOLUME_KEY) {
                double volume = m_gsettings->get(VOLUME_KEY).toDouble();
                //qDebug() << "VolumeShortcut value changed:" << volume;
                changeValue(static_cast<int>(volume));
            } else if(key == MUTE_KEY) {
                mute(m_gsettings->get(MUTE_KEY).toBool());
                //qDebug() << "VolumeShortcut mute state changed: " << m_mute;
            }
        });
    } else {
        qWarning() << "VolumeShortcut:" << "can't find gsettings :" << USD_PLUGIN_MEDIA_KEY_STATE;
        m_isEnable = false;
    }
}

VolumeShortcut::~VolumeShortcut()
{
    if(m_gsettings) {
        delete m_gsettings;
        m_gsettings = nullptr;
    }
}

void VolumeShortcut::active(PluginMetaType::Action action)
{
    Q_UNUSED(action);
}

void VolumeShortcut::setValue(int value)
{
    if(m_gsettings && m_isEnable) {
        if(m_gsettings->keys().contains(VOLUME_KEY, Qt::CaseInsensitive)) {
            m_gsettings->set(VOLUME_KEY, value);
//            qDebug() << "VolumeShortcut set value: " << value;
            if(value > 0 && m_mute) {
                if(m_gsettings->keys().contains(MUTE_KEY, Qt::CaseInsensitive)) {
                    m_gsettings->set(MUTE_KEY, false);
                }
            }
        }
    }
}

const StatusInfo VolumeShortcut::currentStatus()
{
    return m_currentStatusInfo;
}

bool VolumeShortcut::isEnable()
{
    return m_isEnable;
}

void VolumeShortcut::changeValue(int volume)
{
    if(m_currentStatusInfo.getValue() == volume) {
        return;
    }

    updateStatus(volume);
}

QMap<PluginMetaType::SystemMode, PluginMetaData> VolumeShortcut::pluginMetaData()
{
    return m_metaData;
}

void VolumeShortcut::initMetaData()
{
    PluginMetaData pc {true, 1, PluginMetaType::PluginType::ProgressBar};
    PluginMetaData tablet {true, 1, PluginMetaType::PluginType::ProgressBar};
    m_metaData.insert(PluginMetaType::SystemMode::PC, pc);
    m_metaData.insert(PluginMetaType::SystemMode::Tablet, tablet);
}

void VolumeShortcut::mute(bool mute)
{
    m_mute = mute;
    if (mute) {
        m_currentStatusInfo.setIcon(AUDIO_VOLUME_MUTED_SYMBOLIC);
        Q_EMIT statusChanged(m_currentStatusInfo);
    } else {
        updateStatus(m_currentStatusInfo.getValue());
    }
}

void VolumeShortcut::updateStatus(int volume)
{
    if(0 <= volume && volume <= 100) {
        if(volume == 0) {
            m_currentStatusInfo.setIcon(AUDIO_VOLUME_MUTED_SYMBOLIC);
        } else if (volume <= 33) {
            m_currentStatusInfo.setIcon(AUDIO_VOLUME_LOW_SYMBOLIC);
        } else if (volume <= 66) {
            m_currentStatusInfo.setIcon(AUDIO_VOLUME_MEDIUM_SYMBOLIC);
        } else {
            m_currentStatusInfo.setIcon(AUDIO_VOLUME_HIGH_SYMBOLIC);
        }
        m_currentStatusInfo.setValue(static_cast<int>(volume));

    } else {
        qWarning() << "VolumeShortcut value error: " << volume;
        m_currentStatusInfo.setDisable(true);
        m_currentStatusInfo.setIcon(AUDIO_VOLUME_MUTED_SYMBOLIC);
    }

    Q_EMIT statusChanged(m_currentStatusInfo);
}
