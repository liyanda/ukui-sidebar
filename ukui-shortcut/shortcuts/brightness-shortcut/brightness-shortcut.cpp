/*
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: iaom <zhangpengfei@kylinos.cn>
 *
 */
#include "brightness-shortcut.h"

#include <QDebug>
#include <QTranslator>
#include <QApplication>

#define POWER_MANAGER_SETTINGS       "org.ukui.power-manager"
#define BRIGHTNESSAC_KEY                    "brightnessAc"
using namespace UkuiShortcut;

BrightnessShortcut::BrightnessShortcut(QObject *parent) : UkuiShortcutPlugin(parent)
{
    const QByteArray id(POWER_MANAGER_SETTINGS);
    initMetaData();

    if(QGSettings::isSchemaInstalled(id)) {
        m_gsettings = new QGSettings(id);

        if(m_gsettings->keys().contains(BRIGHTNESSAC_KEY, Qt::CaseInsensitive)) {
            // init
            updateStatusInfo(m_gsettings->get(BRIGHTNESSAC_KEY).toDouble());
            m_isEnable = true;
        } else {
            qWarning() << "BrightnessShortcut:" << "can't find key :" << BRIGHTNESSAC_KEY;
            m_isEnable = false;
        }

        connect(m_gsettings, &QGSettings::changed, this, [ & ](const QString & key) {
            if(key == BRIGHTNESSAC_KEY) {
                double brightness = m_gsettings->get(BRIGHTNESSAC_KEY).toDouble();
                qDebug() << "BrightnessShortcut value changed:" << brightness;
                changeValue(brightness);
            }
        });
    } else {
        qWarning() << "BrightnessShortcut:" << "can't find gsettings :" << POWER_MANAGER_SETTINGS;
        m_isEnable = false;
    }

    QTranslator *translator = new QTranslator(this);
    try {
        if(!translator->load(QString(TRANSLATION_FILE_DIR) + "/brightness-shortcut_" + QLocale::system().name() + ".qm")) throw -1;
        QApplication::installTranslator(translator);
    } catch(...) {
        qWarning() << "BrightnessShortcut load translations file" << QLocale::system().name() << "failed!";
    }
    m_currentStatusInfo.setToolTip(tr("Brightness"));
}

BrightnessShortcut::~BrightnessShortcut()
{
    if(m_gsettings) {
        delete m_gsettings;
        m_gsettings = nullptr;
    }
}

QString BrightnessShortcut::pluginId()
{
    return QStringLiteral("BrightnessShortcut");
}

void BrightnessShortcut::active(PluginMetaType::Action action)
{
    Q_UNUSED(action);
}

void BrightnessShortcut::setValue(int value)
{
    if(m_gsettings && m_isEnable) {
        if(m_gsettings->keys().contains(BRIGHTNESSAC_KEY, Qt::CaseInsensitive)) {
            m_gsettings->set(BRIGHTNESSAC_KEY, value);
            qDebug() << "BrightnessShortcut set value: " << value;
        }
    }
}

const StatusInfo BrightnessShortcut::currentStatus()
{
    return m_currentStatusInfo;
}

bool BrightnessShortcut::isEnable()
{
    return m_isEnable;
}

void BrightnessShortcut::changeValue(int brightness)
{
    if(m_currentStatusInfo.getValue() == brightness) {
        return;
    }

    updateStatusInfo(brightness);
}

void BrightnessShortcut::updateStatusInfo(int brightness)
{
    if(0 <= brightness && brightness <= 100) {
//        QString icon = QString("ukui-light-%1.symbolic").arg(QString::number((brightness / 25) * 25));
//        m_currentStatusInfo.setIcon(icon);

        if(brightness == 0) {
            m_currentStatusInfo.setIcon("ukui-light-0-symbolic");
        } else if (brightness <= 25) {
            m_currentStatusInfo.setIcon("ukui-light-25-symbolic");
        } else if (brightness <= 50) {
            m_currentStatusInfo.setIcon("ukui-light-50-symbolic");
        } else if (brightness <= 75) {
            m_currentStatusInfo.setIcon("ukui-light-75-symbolic");
        } else {
            m_currentStatusInfo.setIcon("ukui-light-100-symbolic");
        }
        m_currentStatusInfo.setValue(static_cast<int>(brightness));

    } else {
        qWarning() << "Brightness value error: " << brightness;
        m_currentStatusInfo.setDisable(true);
        m_currentStatusInfo.setIcon("ukui-light-0-symbolic");
    }

    Q_EMIT statusChanged(m_currentStatusInfo);
}

QMap<PluginMetaType::SystemMode, PluginMetaData> BrightnessShortcut::pluginMetaData()
{
    return m_metaData;
}

void BrightnessShortcut::initMetaData()
{
    PluginMetaData pc {true, 0, PluginMetaType::PluginType::ProgressBar};
    PluginMetaData tablet {true, 0, PluginMetaType::PluginType::ProgressBar};
    m_metaData.insert(PluginMetaType::SystemMode::PC, pc);
    m_metaData.insert(PluginMetaType::SystemMode::Tablet, tablet);
}
