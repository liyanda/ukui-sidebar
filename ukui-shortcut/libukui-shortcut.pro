QT += core
VERSION = 1.0.0
DEFINES += VERSION='\\"$${VERSION}\\"'

TEMPLATE = lib
DEFINES += UKUISHOTCUT_LIBRARY
TARGET = ukui-shortcut
CONFIG += c++11 create_pc create_prl no_install_prl no_keywords lrelease

include($$PWD/libukui-shortcut-common.pri)

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

PLUGIN_INSTALL_DIRS = $$[QT_INSTALL_LIBS]/ukui-shortcut-plugins
DEFINES += PLUGIN_INSTALL_DIRS='\\"$${PLUGIN_INSTALL_DIRS}\\"'

QMAKE_CXXFLAGS += -Werror=return-type -Werror=return-local-addr -Werror=uninitialized

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    $$PWD/short-cut-manager.cpp \
    $$PWD/status-info.cpp

HEADERS += \
    $$PWD/short-cut-manager.h \
    $$PWD/status-info.h \
    $$PWD/plugin-common-data.h \
    $$PWD/value-type.h \
    $$PWD/ukui-shortcut-plugin.h \
    $$PWD/ukui-shortcut_global.h

TRANSLATIONS += \
    $$PWD/translations/ukui-shortcut_zh_CN.ts

TRANSLATION_FILE_DIR = $${SHORTCUT_DATA_INSTALL_DIR}
DEFINES += TRANSLATION_FILE_DIR='\\"$${TRANSLATION_FILE_DIR}\\"'

# 安装在编译过程中才会生成的文件需要指定 'no_check_exist'，否则不会在Makefile中生成对应的install规则
qm_files.CONFIG += no_check_exist
qm_files.path = $${TRANSLATION_FILE_DIR}

# 对每一个翻译文件生成对应的 make install 规则
for(file, TRANSLATIONS) {
    # 提取每一个.ts文件的文件名
    TS_FILE = $$basename(file)
    # 转换对应的.qm文件
    QM_FILE = $$replace(TS_FILE, ".ts", ".qm")

    qm_files.files += $${OUT_PWD}/.qm/$${QM_FILE}
    # message(QM_FILE = $${QM_FILE})
}

INSTALLS += qm_files

#rules for deployment.
unix {
    target.path = $$[QT_INSTALL_LIBS]
    QMAKE_PKGCONFIG_NAME = ukui-shortcut
    QMAKE_PKGCONFIG_DESCRIPTION = ukui-shortcut Header files
    QMAKE_PKGCONFIG_VERSION = $$VERSION
    QMAKE_PKGCONFIG_LIBDIR = $$target.path
    QMAKE_PKGCONFIG_DESTDIR = pkgconfig
    QMAKE_PKGCONFIG_INCDIR = /usr/include/ukui-shortcut
    QMAKE_PKGCONFIG_CFLAGS += -I/usr/include/ukui-shortcut
#    QMAKE_PKGCONFIG_REQUIRES = Qt5Widgets

    INSTALLS += target

    header.path = /usr/include/ukui-shortcut
    header.files += *.h
    header.files += development-files/header-files/*

    INSTALLS += header
}
